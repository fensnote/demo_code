#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include "opencv2/opencv.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc/types_c.h>
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>

using namespace std;
using namespace cv;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void InitVideo();

private slots:
    void on_play_clicked();
    void playTimer();

    void on_stop_clicked();

private:
    Ui::MainWindow *ui;
    QTimer *m_pTimer;
    VideoCapture *m_pVideo;
};

#endif // MAINWINDOW_H
