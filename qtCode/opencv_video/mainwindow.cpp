#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc/types_c.h>
using namespace cv;
using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_pTimer = new QTimer;
    m_pTimer->setInterval(30);
    connect(m_pTimer, &QTimer::timeout, this, &MainWindow::playTimer);

    ui->play->setEnabled(true);
    ui->stop->setEnabled(false);

    InitVideo();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::playTimer()
{
    Mat frame;

    //从cap中读取一帧数据，存到fram中
    *m_pVideo >> frame;
    if ( frame.empty() )
    {
        return;
    }
    cv::cvtColor(frame, frame, COLOR_BGR2RGB);//图像格式转换
    QImage disImage = QImage((const unsigned char*)(frame.data),frame.cols,frame.rows,frame.cols*frame.channels(),QImage::Format_RGB888);
    ui->label->setPixmap(QPixmap::fromImage(disImage));//显示图像
}

void MainWindow::InitVideo()
{
    m_pVideo = new VideoCapture("test.mp4");

}

void MainWindow::on_play_clicked()
{
    m_pTimer->start();
    ui->play->setEnabled(false);
    ui->stop->setEnabled(true);

}

void MainWindow::on_stop_clicked()
{
    ui->play->setEnabled(true);
    ui->stop->setEnabled(false);

    m_pTimer->stop();
}
