#ifndef NINEBLOCK_H
#define NINEBLOCK_H

#include <QWidget>
//#include "ui_nineblock.h"

class CSliderLock : public QWidget
{
    Q_OBJECT

public:
    enum PasswdAuthState
    {
        PasswdAuthError=-1,
        PasswdAuthNormal= 0,
        PasswdAuthOk,
    };

    CSliderLock(QWidget *parent = 0, Qt::WindowFlags flags = 0);
    ~CSliderLock();

    //清空上次的输入
    void clear();

    //设置成功
    void setSuccess(CSliderLock::PasswdAuthState state);



signals:
    void sigInputFinished(const QString &userInput);
    void sigPressed();

private:
    virtual void paintEvent(QPaintEvent *event);
    virtual void resizeEvent(QResizeEvent *event);

    //鼠标支持
    void mouseDoubleClickEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);


    void calculate();
    int inside(QPoint pos);

private:
    QPoint m_blocks[9];
    int m_radius;
    bool m_bMousePessed;
    QPoint m_curPoint;

    //密码
    char m_input[64];			//用户输入
    char m_status[9];
    bool m_bInside;			    //当前鼠标是否在圆内
    int m_nCorrect;				//结束是否正确
    int m_nSuccess;				//错误为红色	-1：错误，0：常态，1：正确
};

#endif // NINEBLOCK_H
