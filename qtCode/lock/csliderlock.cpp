#include <QPainter>
#include <QMouseEvent>
#include <QDebug>
#include <QtMath>
#include "csliderlock.h"

CSliderLock::CSliderLock(QWidget *parent, Qt::WindowFlags flags)
	: QWidget(parent, flags)
{
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);

    resize(350, 350);
	
	m_bMousePessed = false;
	memset(m_input, 0, sizeof(m_input));
	memset(m_status, 0, sizeof(m_status));
	m_nSuccess = 0;
}

CSliderLock::~CSliderLock()
{

}

//清空上次的输入
void CSliderLock::clear()
{
	memset(m_input, 0, sizeof(m_input));
	memset(m_status, 0, sizeof(m_status));
	m_bInside = false;
	m_nSuccess = 0;
	update();
}

//设置成功
void CSliderLock::setSuccess(CSliderLock::PasswdAuthState state)
{
    m_nSuccess = state;
	update();
}

void CSliderLock::calculate()
{
	QRect rect(0, 0, width(), height());

	//确定9宫格的位置
	int framesize = 300;
	QRect target(0, 0, framesize, framesize);
	target.moveCenter(rect.center());

	//每一个格的中心点
	int size = framesize / 3;
	int y = size / 2 + target.top();
	int x = 0;

	for (int i = 0; i < 3; i++)
	{
		x = size / 2 + target.left();
		for (int j = 0; j < 3; j++)
		{
			QPoint &c = m_blocks[i*3 + j];
			c.setX(x);
			c.setY(y);

			x += size;
		}
		y += size;
	}

	//圆半径
    m_radius = size / 3 - 4;
}

int CSliderLock::inside(QPoint pos)
{
	for (int i = 0; i < 9; i++)
	{
		double x = m_blocks[i].x() - pos.x();
		double y = m_blocks[i].y() - pos.y();
		double distance = sqrt(x*x + y*y);
		if (distance < m_radius)
		{
			return i;
		}
	}

	return -1;
}

void CSliderLock::resizeEvent(QResizeEvent *event)
{
	calculate();
}

void CSliderLock::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);

	QRect rect(0, 0, width(), height());

	QPen pen;
	
	//背景
	painter.setPen(Qt::NoPen);
    //painter.setBrush(QBrush(QColor(0xFF, 0xFF, 0xFF)));
	painter.drawRect(rect);

	//画线
    QColor colorNormal(34, 138, 34);
	QColor colorError(0XFF, 0x00, 0x00);
	QColor colorOK(0x00, 0xFF, 0x00);
	QColor clr = colorNormal;

    if (m_nSuccess > 0)
	{
		clr = colorOK;
	}
	if (m_nSuccess < 0)
	{
		clr = colorError;
	}


    pen.setStyle(Qt::SolidLine);
    pen.setJoinStyle(Qt::RoundJoin);
    pen.setColor(clr);
    pen.setWidth(10);
	painter.setPen(pen);
	
	int len = strlen(m_input);
	QPoint p1;
	for (int i = 0; i < len && i < 9; i++)
	{
		int index = m_input[i] - '1';
		QPoint p = m_blocks[index];
		if (i>0)
		{
			painter.drawLine(p1, p);
		}
		p1 = p;
	}

    if ( m_bMousePessed && !p1.isNull())
        painter.drawLine(p1, m_curPoint);

    //画图
//    pen.setColor(clr);
    pen.setWidth(5);
    painter.setPen(Qt::NoPen);
    painter.setBrush(QColor(0xaa, 0xaa, 0xaa));
    for (int i = 0; i < 9; i++)
    {
        QPoint &c = m_blocks[i];
        if (m_status[i])
        {
            painter.setPen(pen);
            painter.drawEllipse(c, m_radius, m_radius);
            painter.setBrush(QBrush(clr));
            painter.drawEllipse(c, m_radius/4, m_radius/4);
            painter.setBrush(QBrush(QColor(0xAA, 0xAA, 0xAA)));
        }
        else
        {
            painter.setPen(Qt::NoPen);
            painter.setBrush(QBrush(QColor(0xAA, 0xAA, 0xAA)));
            painter.drawEllipse(c, m_radius, m_radius);
        }

    }


}

//忽略双击事件
void CSliderLock::mouseDoubleClickEvent(QMouseEvent *event)
{
	QWidget::mouseDoubleClickEvent(event);
}

void CSliderLock::mouseMoveEvent(QMouseEvent *event)
{
	if (m_bMousePessed)
	{
		QPoint pos = event->pos();
        m_curPoint = pos;
		int sel = inside(pos);
		if (sel < 0)		//不在任何一个圆内
		{
			m_bInside = false;
		}
		else
		{
			if (!m_bInside)
			{
				//在圆内划动时，不重复计算
				m_bInside = true;

				if (m_status[sel])		//不重复加入
				{
					return;
				}

				//附加一个字节
				char number[2];
				number[0] = '1' + sel;
				number[1] = 0;
				strcat(m_input, number);

				//将标志位设为1，表示途径该圆
				m_status[sel] = 1;
			}
		}

		update();	//重绘
	}
}

void CSliderLock::mousePressEvent(QMouseEvent *event)
{
    if ( m_bMousePessed == false )
    {
        sigPressed();
    }
	m_bMousePessed = true;

	//清空密码
	clear();
}

void CSliderLock::mouseReleaseEvent(QMouseEvent *event)
{
	m_bMousePessed = false;
	update();

	//用户没有输入
	if (0 == strlen(m_input))		
	{
		return;
	}

	//外部调用可以响应这个信号进行处理
    emit sigInputFinished(QString(m_input));
}



