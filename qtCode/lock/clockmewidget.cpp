#include "clockmewidget.h"
#include "ui_clockmewidget.h"
#include <QDebug>

CLockMeWidget::CLockMeWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CLockMeWidget)
{
    ui->setupUi(this);
    this->setProperty("form", true);
#ifdef Q_OS_WIN32
    this->setProperty("canMove", true);
#endif
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);


    connect(ui->lockMeWidget, &CSliderLock::sigInputFinished, this, &CLockMeWidget::onInputEnd);
    connect(ui->lockMeWidget, &CSliderLock::sigPressed, this, [this](){
        ui->labelResult->clear();
    });
}

CLockMeWidget::~CLockMeWidget()
{
    delete ui;
}

void CLockMeWidget::on_concel_clicked()
{
    close();
}

void CLockMeWidget::onInputEnd(QString str)
{
    setPasswdAuthResult(CSliderLock::PasswdAuthError);
    emit sigInputEnd(str);
}

void CLockMeWidget::setPasswdAuthResult(CSliderLock::PasswdAuthState state)
{
     ui->lockMeWidget->setSuccess(state);
     switch (state)
     {
    case CSliderLock::PasswdAuthError:
        ui->labelResult->setText("图案错误,请重试");
        break;

     case CSliderLock::PasswdAuthOk:
         ui->labelResult->setText("解锁成功");
         break;
     }
}

void CLockMeWidget::start()
{
    ui->lockMeWidget->clear();
    ui->labelResult->clear();
    show();
}

