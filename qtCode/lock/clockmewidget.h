#ifndef CLOCKMEWIDGET_H
#define CLOCKMEWIDGET_H

#include <QWidget>
#include "csliderlock.h"

namespace Ui {
class CLockMeWidget;
}

class CLockMeWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CLockMeWidget(QWidget *parent = 0);
    ~CLockMeWidget();
    void setPasswdAuthResult(CSliderLock::PasswdAuthState state);
    void start();

signals:
    void sigInputEnd(QString);

private slots:
    void on_concel_clicked();
    void onInputEnd(QString str);

private:
    Ui::CLockMeWidget *ui;
};

#endif // CLOCKMEWIDGET_H
