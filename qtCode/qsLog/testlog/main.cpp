﻿#include "widget.h"
#include <QApplication>

#include "../QsLog/QsLog.h"
using namespace QsLogging;

#define LOG_FILE_PATH "log/"

void initLog()
{
    // 初始化日志机制
    Logger& logger = Logger::instance();
    logger.setLoggingLevel(QsLogging::TraceLevel);

    // 添加文件为目的地
    const QString sLogPath(LOG_FILE_PATH+QString("app.log"));
    DestinationPtr fileDestination(DestinationFactory::MakeFileDestination(
      sLogPath, EnableLogRotation, MaxSizeBytes(10*1024), MaxOldLogCount(3)));
    logger.addDestination(fileDestination);

    DestinationPtr debugDestination(DestinationFactory::MakeDebugOutputDestination());
    logger.addDestination(debugDestination);

    // 打印日志
    QLOG_FATAL() << "app start."<<sLogPath;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    initLog();

    Widget w;



    w.show();

    return a.exec();
}
