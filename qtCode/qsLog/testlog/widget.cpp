﻿#include "widget.h"
#include "ui_widget.h"
#include <QTimer>
#include "../QsLog/QsLog.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    QTimer *pTimer = new QTimer;
    pTimer->setInterval(500);
    connect(pTimer, &QTimer::timeout, this, [this](){

        QLOG_FATAL() << "fatal msg.";
        QLOG_ERROR() << "error msg.";
        QLOG_WARN()  << "warn  msg.";
        QLOG_INFO()  << "info  msg.";
        QLOG_DEBUG() << "debug msg.";
        QLOG_TRACE() << "trace msg.";


    });
    pTimer->start();
}

Widget::~Widget()
{
    delete ui;
}

