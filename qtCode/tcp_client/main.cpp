﻿#include <QApplication>
#include <QTextCodec>
#include "widget.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;
    //获取系统编码-解决在win7中文显示正常，在xp下中文乱码问题
//    QTextCodec *codec = QTextCodec::codecForName("System");
//    QTextCodec::setCodecForLocale(codec);
//    QTextCodec::setCodecForCStrings(codec);
//    QTextCodec::setCodecForTr(codec);


    w.show();
    
    return a.exec();
}
