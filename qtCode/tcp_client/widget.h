﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QtNetwork>
#include <QMessageBox>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT
    
public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    
private slots:
    void on_closeButton_clicked();

    void on_connectButton_clicked();

    void on_sendButton_clicked();

    void on_pushButton_clicked();

    void on_clearSendBuf_clicked();

    void on_allSelectButton_clicked();

    void on_hexRadioButton_clicked();

    void on_asciiRadioButton_2_clicked();

    void on_sendMsg_textChanged(const QString &arg1);

    void on_disConnectButton_clicked();

public slots:

    void dataReceived();
    void slotDisconnected();
private:
    Ui::Widget *ui;
    bool bConnectFlag;
    bool bHexMod; //true=Hex mode ; false=Ascii mode
    QTcpSocket *tcpSocket;

};

#endif // WIDGET_H
