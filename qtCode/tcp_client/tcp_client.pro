#-------------------------------------------------
#
# Project created by QtCreator 2013-06-26T21:07:06
#
#-------------------------------------------------

QT       += core  gui network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tcp_client
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h

FORMS    += widget.ui

RC_FILE=img/myapp.rc
