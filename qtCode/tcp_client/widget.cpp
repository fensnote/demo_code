﻿#include "widget.h"
#include "ui_widget.h"
#include "stdio.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    setWindowTitle(QObject::tr("tcp客户端调试助手"));

    bConnectFlag = false;
    ui->recvMsg->setReadOnly(true);

    bHexMod = false;
    ui->hexRadioButton->setChecked(false);
    ui->asciiRadioButton_2->setChecked(true);


    tcpSocket = new QTcpSocket(this);
    connect(tcpSocket, SIGNAL(readyRead()),this, SLOT(dataReceived()));
    connect(tcpSocket, SIGNAL(disconnected()),this, SLOT(slotDisconnected()));
    connect(tcpSocket, &QTcpSocket::connected, this, [this](){

        ui->svrIP->setEnabled(false);
        ui->svrPort->setEnabled(false);
        ui->connectButton->setEnabled(false);
        ui->disConnectButton->setEnabled(true);
        ui->msg->setText("连接成功!");
    });
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_closeButton_clicked()
{
    this->close();
}

void Widget::on_connectButton_clicked()
{
    tcpSocket->disconnectFromHost();
    tcpSocket->close();

    QString ip=ui->svrIP->text();

    QHostAddress serverIP;
    serverIP.setAddress(ip);

    int port = ui->svrPort->text().toInt();
    tcpSocket->connectToHost(serverIP,port);
}
//数据接收处理
void Widget::dataReceived()
{
    while (tcpSocket->bytesAvailable()>0)
    {
        QByteArray datagram;
        datagram.resize(tcpSocket->bytesAvailable());

        tcpSocket->read(datagram.data(), datagram.size());

        ui->recvMsg->append("****************************");

        if(bHexMod)
            ui->recvMsg->append(datagram.toHex());
        else
            ui->recvMsg->append(datagram.data());
        ui->recvMsg->append("****************************");
        //ui->sendMsg->setText(datagram.toHex());
    }
}
//发送键处理函数
void Widget::on_sendButton_clicked()
{
    int ret = 0;
    if(bHexMod)
    {
        QByteArray datagram;
        datagram = QByteArray::fromHex(ui->sendMsg->text().toLatin1());
        ret = tcpSocket->write(datagram);
    }
    else
        ret = tcpSocket->write(ui->sendMsg->text().toLatin1());
    if(ret < 0)    
        ui->msg->setText("发生失败: 无效连接!");
    else
        ui->msg->setText("发送成功!");
}


//检测到断开连接时的槽处理函数
void Widget::slotDisconnected()
{
    //tcpSocket->disconnectFromHost();
    ui->svrIP->setEnabled(true);
    ui->svrPort->setEnabled(true);
    ui->connectButton->setEnabled(true);

    bConnectFlag = false;
//    QMessageBox::information(this,tr("error"),tr("连接已断开!"));
    ui->msg->setText("连接已断开!");
}

void Widget::on_pushButton_clicked()
{
    ui->recvMsg->clear();
}

void Widget::on_clearSendBuf_clicked()
{
    ui->sendMsg->clear();
}

void Widget::on_allSelectButton_clicked()
{
    ui->recvMsg->selectAll();
}

void Widget::on_hexRadioButton_clicked()
{
    bHexMod = true;
}

void Widget::on_asciiRadioButton_2_clicked()
{
    bHexMod = false;
}


void Widget::on_sendMsg_textChanged(const QString &arg1)
{
    if(bHexMod)
    {
        //ui->recvMsg->append(arg1);
    }
}

void Widget::on_disConnectButton_clicked()
{
    ui->svrIP->setEnabled(true);
    ui->svrPort->setEnabled(true);
    tcpSocket->abort();
    tcpSocket->close();
    bConnectFlag = false;
    ui->connectButton->setEnabled(true);

}
