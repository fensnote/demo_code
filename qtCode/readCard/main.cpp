﻿#include <QCoreApplication>
#include "sdtapi.h"
#include <QDebug>
#include <QTextEncoder>
#include <iostream>
#include <stdio.h>

int readCard()
{
    int ret;
    int iPort=1;

    //初始化
    ret=InitComm(iPort);
    if ( ret )
    {
        //认证
        ret= Authenticate ();
        if (ret)
        {
//            ReadBaseMsg接口调用测试；

            char Msg[200];
            ret= ReadBaseMsg ((unsigned char*)Msg, 0 );
            qDebug()<<"readBaseMsg: "<<ret<<", data: "<<QByteArray(Msg, 200).toHex();

            if (ret > 0 ){
            //显示文字及图片信息
                char buf[64] = {0};
                QByteArray data = QByteArray(Msg, 200);
//                QString strUnicode;
//                QByteArray strUtf8;
//                QTextCodec* gbk = QTextCodec::codecForName("gbk"); //读卡器读出来的中文都是国标编码，如果需要UTF-8的编码，这里可以通过一下步骤转换为UTF-8编码
//                QTextCodec *utf8 = QTextCodec::codecForName("UTF-8");

                //姓名
//                memcpy(buf, data.left(31).data(), 31);
////                strUnicode = gbk->toUnicode(buf);
////                strUtf8 = utf8->fromUnicode(strUnicode);
//                std::cout<<"name: "<<buf<<std::endl;
//                std::cout<<"sex : "<<data.mid(31, 3).data()<<std::endl;


            }


            //ReadBaseInfos接口调用测试
            char name[32] = {0};
            char sex[8] = {0};
            char folk[8] = {0};
            char birth[16] = {0};
            char code[32]  = {0};
            char addr[128] = {0};
            char agency[32]= {0};
            char start[16]  = {0};
            char end[16]  = {0};

            printf("-----------------------------------------\n");
            ret= ReadBaseInfos (name, sex,folk,birth,code,addr,agency,start,end );
            if (ret > 0 )
            {
                printf("name: %s\nsex : %s\n民族: %s\n生日: %s\n身份证号: %s\n地址: %s\n签发机关: %s\n有效期开始: %s\n有效期结束: %s\n",name, sex,folk,birth,code,addr,agency,start,end);
            }

        }
    }

    ret= CloseComm();
    return ret;

}
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    readCard();

    return a.exec();
}
