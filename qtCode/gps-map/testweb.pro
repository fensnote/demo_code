#-------------------------------------------------
#
# Project created by QtCreator 2018-01-25T13:00:32
#
#-------------------------------------------------

QT       += core gui script webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = testweb
TEMPLATE = app


SOURCES += main.cpp\
        testwebmap.cpp

HEADERS  += testwebmap.h

FORMS    += testwebmap.ui

RC_FILE = ./img/myapp.rc

RESOURCES += \
    myqrc.qrc