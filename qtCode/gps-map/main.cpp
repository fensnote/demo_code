#include "testwebmap.h"
#include <QTextCodec>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    testWebMap w;
    QTextCodec *codec = QTextCodec::codecForName("System");
    QTextCodec::setCodecForLocale(codec);
    w.setWindowTitle(QObject::tr("GPS定位"));
    w.show();

    return a.exec();
}
