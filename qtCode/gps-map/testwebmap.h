#ifndef TESTWEBMAP_H
#define TESTWEBMAP_H

#include <QMainWindow>
#include <QtNetwork>


namespace Ui {
class testWebMap;
}
typedef struct gpsData
{
    char lo[16];
    char la[16];
}gpsDataSt;


class testWebMap : public QMainWindow
{
    Q_OBJECT

public:
    explicit testWebMap(QWidget *parent = 0);
    ~testWebMap();
    void convertGpsData(QString srcData, QString &destData, int pos);

public slots:

    void addObjectToJs();
    void doJavascript2(QString str);

    void on_modifyButton_clicked();

    void acceptDevConnect();
    void onDataRecv();
    void disConnectEvent();
    void about();
private slots:


    void on_svrButtn_clicked();

    void on_reloadUrl_clicked();

private:
    QTcpServer *m_pTcpServer;   //监听远程设备的连接
    QTcpSocket *m_pTcpClient; //监听本地调试程序的连接
    bool bSvrStart;
    bool bClientVaild;
    Ui::testWebMap *ui;
};

#endif // TESTWEBMAP_H
