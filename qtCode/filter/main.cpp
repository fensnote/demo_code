// main.cpp
#include <QApplication>
#include <QTableView>
#include "mainwindow.h"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

    MainWindow mainWindow;
    mainWindow.show();
    mainWindow.init();

    return app.exec();
}

