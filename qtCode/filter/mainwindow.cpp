#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>




MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::init()
{
    // 创建一个标准模型
    QStandardItemModel *model = new QStandardItemModel(5, 2);
    for (int i = 0; i < model->rowCount(); ++i)
    {
        for (int j = 0; j < model->columnCount(); ++j)
        {
            model->setItem(i, j, new QStandardItem(QString("%1").arg(i*2+j)));
            qDebug("i: %d, j: %d\n", i, j);
        }
    }

    // 创建代理模型
    proxyModel = new QSortFilterProxyModel(this);
    proxyModel->setSourceModel(model);
    proxyModel->setSortCaseSensitivity(Qt::CaseInsensitive);
    proxyModel->setFilterKeyColumn(0);  //指定列,从0开始

    // 设置表格视图的模型为代理模型
    ui->tableView->setModel(proxyModel);

    // 将行编辑器的文本改变信号连接到槽函数，更新过滤
    connect(ui->lineEdit, &QLineEdit::textChanged, this, &MainWindow::updateFilter);
}
