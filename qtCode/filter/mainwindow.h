#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <QRegExp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void init();


public slots:
    void updateFilter(const QString &pattern) {
        // 设置代理模型的过滤规则
        QRegExp regExp(pattern);
        proxyModel->setFilterRegExp(regExp);
    }

private:
    Ui::MainWindow *ui;
    QSortFilterProxyModel *proxyModel;

};

#endif // MAINWINDOW_H
