﻿#include "dataformat.h"
#include "ui_dataformat.h"
#include "xlsxdocument.h"
#include <QFile>
#include <QDebug>
#include "xlsxcellreference.h"
#include <QDateTime>
#include <QFileDialog>

DataFormat::DataFormat(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DataFormat)
{
    ui->setupUi(this);

//    CreatExcel();
}

DataFormat::~DataFormat()
{
    delete ui;
}


void DataFormat::on_convert_clicked()
{

    QXlsx::Document xlsx;

    xlsx.setColumnWidth(1,7,12);  //设置列宽

    QXlsx::Format centerAlign;
    centerAlign.setHorizontalAlignment(QXlsx::Format::AlignHCenter); //设置横向、纵向居中
    centerAlign.setVerticalAlignment(QXlsx::Format::AlignVCenter);   //

    centerAlign.setFontBold(true);          //字体加粗
    centerAlign.setFontColor(Qt::blue);     //设置字体颜色
    centerAlign.setFontSize(30);            //设置字体大小
    //设置上下左右的线类型
    centerAlign.setTopBorderStyle(QXlsx::Format::BorderThin);
    centerAlign.setBottomBorderStyle(QXlsx::Format::BorderThin);
    centerAlign.setLeftBorderStyle(QXlsx::Format::BorderThin);
    centerAlign.setRightBorderStyle(QXlsx::Format::BorderThin);

    xlsx.mergeCells(1,1,1,7,centerAlign); //合并单元表格
    xlsx.write("A1", "测试结果报表");       //总标题


    centerAlign.setFontColor(Qt::black);  //设置字体颜色
    centerAlign.setFontSize(13);          //设置字体大小
    xlsx.mergeCells(2,1,1,7,centerAlign); //副标题合并单元表格，2-第二行，1-7列合并
    xlsx.write("A2", "1号实验报表");        //副标题

    xlsx.mergeCells(3,1,1,2,centerAlign);   //合并单元表格
    xlsx.write("A3","测试人：",centerAlign);  //
    xlsx.mergeCells(3,3,1,2,centerAlign);
    xlsx.write("C3","结果： 合格");

    xlsx.mergeCells(3,5,1,3,centerAlign);
    xlsx.write("E3", "检测时间：2020-12-13",centerAlign);

    centerAlign.setFontBold(false);
    centerAlign.setFontSize(10);


    //填充测试数据单元表格
    for (int row = 4; row < 50; row++)
    {
        for (int col = 0; col < 7; col++ )
        {
            xlsx.write(row,col+1, col,centerAlign);
        }
    }
    // save file.
    QString strPath = "test.xlsx";
    xlsx.saveAs(strPath);

}

