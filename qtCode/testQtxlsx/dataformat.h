﻿#ifndef DATAFORMAT_H
#define DATAFORMAT_H

#include <QWidget>
#include <QMap>
#include <QVector>

namespace Ui {
class DataFormat;
}

class DataFormat : public QWidget
{
    Q_OBJECT

public:
    explicit DataFormat(QWidget *parent = 0);
    ~DataFormat();

private:

private slots:
    void on_convert_clicked();


private:
    Ui::DataFormat *ui;

};

#endif // DATAFORMAT_H
