﻿#include <QDebug>
#include "cdatabase.h"
#include <QTimer>

CDataBase::CDataBase(QObject *parent) : QObject(parent)
{
    m_pDbHandle = DbHandle::getInstance();

//    init();
}

CDataBase::~CDataBase()
{

}

void CDataBase::init(const QString &name)
{
    bool ret = m_pDbHandle->creatConnection(name);
    if ( ret )
    {
        qDebug()<<"CDataBase::init, open config db succ!";
    }
    else
    {
        qDebug()<<"CDataBase::init, open config db fault!";
    }
}



bool CDataBase::insert(const QString& dbName, QString table,const QVariantList &data)
{
    bool ret = false;
    for ( int i = 0; i < data.size(); i++ )
    {
        ret = m_pDbHandle->insertRecord(dbName, table, data.at(i).toMap());
        if ( ret == false )
        {
            //error
            break;
        }
    }
    return ret;
}

/***************************************************************
* @function name:
* @摘要             更新数据
* @输入参数          dbName -- 数据库文件
*                   table  -- 数据库表名称
*                   filter -- 过滤条件 多个记录的过滤条件list
*                   data   -- 要更新的记录，支持一次传入过个记录，list
* @输出参数          无
* @返回值            void
* @author           wuquanwei
* @date             2021-02-25
*过滤条件示例
QVariantList vlist;
QVariantMap vmap;
vmap["filter"] = "id=1";
vlist << vmap;
**************************************************************/
bool CDataBase::update(const QString& dbName, QString table, const QVariantList &filter, const QVariantList &data)
{
    bool ret = false;
    for ( int i = 0; i < filter.size(); i++ )
    {
//        qDebug()<<"i: "<<i<<", filter: "<<filter.at(i).toMap()["filter"].toString()<<", val: "<<data.at(i).toMap();
        ret = m_pDbHandle->updateRecord(dbName, table, filter.at(i).toMap()["filter"].toString(), data.at(i).toMap());
        if ( ret == false )
        {
            //error
            break;
        }
    }

    return ret;
}
bool CDataBase::update(const QString& dbName, const QString &table, const QString &filter, const QVariantMap &data)
{
      return m_pDbHandle->updateRecord(dbName, table, filter, data);
}
QVariantList CDataBase::getData(const QString& dbName, QString sql)
{
    return m_pDbHandle->getAllBySelect(dbName,sql);
}

QVariantList CDataBase::getOneData(const QString& dbName, QString sql)
{
    return m_pDbHandle->getOneBySelect(dbName,sql);
}

bool CDataBase::execCmd(const QString& dbName, QString sql)
{
    return m_pDbHandle->execCmd(dbName,sql);
}

QString CDataBase::getValue(const QString& dbName, const QString &key)
{
    return m_pDbHandle->getParameter(dbName, key);
}

bool CDataBase::setValue(const QString& dbName, const QString &key,const QString &value)
{
    return m_pDbHandle->setParameter(dbName, key, value);
}
