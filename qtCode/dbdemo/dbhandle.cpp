﻿#include "dbhandle.h"
#include <memory>
#include <QDebug>
#include <QSqlQuery>
#include <QDateTime>
#include <QSqlError>
#include <QProcess>
#include <QPluginLoader>

DbHandle::DbHandle(QObject *parent) : QObject(parent)
{
//   dbHandle = QSqlDatabase::addDatabase("QSQLITE");
}

DbHandle::~DbHandle()
{
//    for ( auto iter = m_dbMap.begin(); iter != m_dbMap.end(); iter++)
//    {
//        closeConnection(iter.key());
//    }

//    m_dbMap.clear();
}

DbHandle *DbHandle::getInstance()
{
    static DbHandle dbhandle;
    return &dbhandle;
}

void DbHandle::closeConnection(const QString& dbName)
{
    auto iter = m_dbMap.find(dbName);
    if ( iter == m_dbMap.end() )
        return;

    iter.value()->close();
}

bool DbHandle::creatConnection(const QString &ip, const QString &port, const QString &username, const QString &passwd, const QString& dbName)
{
    QSqlDatabase *pDb = new QSqlDatabase(QSqlDatabase::addDatabase("QMYSQL", dbName));
    pDb->setHostName(ip);
    pDb->setDatabaseName(dbName);       //这里输入你的数据库名
    if (username.isEmpty())
    {
        pDb->setUserName("depot_ms");
    }
    else
        pDb->setUserName(username);
    pDb->setPassword(passwd);   //这里输入你的密码

    m_dbMap[dbName] = pDb;

    if ( !openDb(dbName) )
    {
        qDebug()<<"Database Error: "<<pDb->lastError().text();

        return false;
    }

    return true;
}

//sqlite db
bool DbHandle::creatConnection(const QString& dbName)
{
    QSqlDatabase *pDb = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE", dbName));
//    m_dbFileName = dbName;

    m_dbMap[dbName] = pDb;

    pDb->setDatabaseName(dbName);
    if ( !openDb(dbName) )
    {
        qDebug()<<"Database Error: "<<pDb->lastError().text();

        return false;
    }

    return true;
}

/***************************************************************
* @function name:   DbHandle::open
* @摘要             根据数据库名称打开数据库
* @输入参数          无
* @输出参数          无
* @返回值            bool true-打开成功；false - 打开失败
* @author           wuquanwei
* @date             2021-01-21
**************************************************************/
bool DbHandle::openDb(const QString &dbName)
{
    auto iter = m_dbMap.find(dbName);
    if ( iter == m_dbMap.end() )
        return false;

    return iter.value()->open();
}

/***************************************************************
* @function name:   DbHandle::isDbOpen
* @摘要             查询指定数据库是否打开了
* @输入参数          无
* @输出参数          无
* @返回值            bool true-已经打开；false - 关闭
* @author           wuquanwei
* @date             2021-01-21
**************************************************************/
bool DbHandle::isDbOpen(const QString &dbName)
{
    auto iter = m_dbMap.find(dbName);
    if ( iter == m_dbMap.end() )
        return false;

    return iter.value()->isOpen();
}

QString DbHandle::getParameter(const QString& dbName, QString name)
{
    auto iter = m_dbMap.find(dbName);
    if ( iter == m_dbMap.end() )
        return "";

    QSqlDatabase *pDb = iter.value();
    if ( !pDb->isOpen() )
    {
        qDebug()<<"db: "<<dbName<<" is not open.";
        return "";
    }

    QSqlTableModel tbl(this,*pDb);
    tbl.setTable("sys_cfg");
    tbl.setFilter("user='"+name+"'");
    if(tbl.select())
    {
        if(tbl.rowCount()>=1)
        {
            QString inStr = tbl.record(0).value("passwd").toString();
            return inStr;
        }
    }
    else
        qDebug()<<tbl.lastError();

    return "";
}

bool DbHandle::setParameter(const QString& dbName, QString name,QString value)
{
    auto iter = m_dbMap.find(dbName);
    if ( iter == m_dbMap.end() )
        return false;

    QSqlDatabase *pDb = iter.value();
    if ( !pDb->isOpen() )
    {
        qDebug()<<"db: "<<dbName<<" is not open.";
        return false;
    }

    QSqlTableModel tbl(this,*pDb);
    tbl.setTable("sys_cfg");
    tbl.setFilter("user='"+name+"'");
    if(tbl.select())
    {
        if(tbl.rowCount()>=1)
        {
            QSqlRecord record = tbl.record(0);
            record.setValue("passwd",value);
            tbl.setRecord(0,record);
            return tbl.submitAll();
        }
        else
        {
//            qDebug()<<"not find key: "<<name<<"in sys_cfg table, now creat.";
            int rowNum = tbl.rowCount();
            tbl.insertRow(rowNum);
            tbl.setData(tbl.index(rowNum,0),name);
            tbl.setData(tbl.index(rowNum,1),value);
            return tbl.submitAll();
        }
    }
    else
        qDebug()<<tbl.lastError();


    return false;
}

QByteArray DbHandle::getBlob(const QString& dbName, QString sql)
{
    QByteArray res;
    auto iter = m_dbMap.find(dbName);
    if ( iter == m_dbMap.end() )
        return res;

    QSqlDatabase *pDb = iter.value();
    if ( !pDb->isOpen() )
    {
        qDebug()<<"db: "<<dbName<<" is not open.";
        return res;
    }

    QSqlQuery query(*pDb);
    query.exec(sql);

    while(query.next())
    {
        return query.value(0).toByteArray();
    }

    return res;
}



QVariantList DbHandle::getAllBySelect(const QString& dbName, QString sql)
{
    QVariantList datalist;
    auto iter = m_dbMap.find(dbName);
    if ( iter == m_dbMap.end() )
        return datalist;

    QSqlDatabase *pDb = iter.value();
    if ( !pDb->isOpen() )
    {
        qDebug()<<"db: "<<dbName<<" is not open.";
        return datalist;
    }

    QSqlQuery query(*pDb);
    query.exec(sql);

    while(query.next())
    {
        int i;
        QVariantMap data;
        for(i=0;i<query.record().count();i++)
        {
            data.insert(query.record().fieldName(i),query.record().value(i));
//                qDebug()<<"as "<<query.record().fieldName(i)<<query.record().value(i);
        }
        datalist.append(data);
    }

    return datalist;
}

QVariantList DbHandle::getOneBySelect(const QString& dbName, QString sql)
{
    QVariantList datalist;
    auto iter = m_dbMap.find(dbName);
    if ( iter == m_dbMap.end() )
        return datalist;

    QSqlDatabase *pDb = iter.value();
    if ( !pDb->isOpen() )
    {
        qDebug()<<"db: "<<dbName<<" is not open.";
        return datalist;
    }

    QSqlQuery query(*pDb);
    query.exec(sql);

    while(query.next())
    {
        int i;
        QString str;
        for(i=0;i<query.record().count();i++)
        {
            str += query.record().value(i).toString();
            str += "|";
        }
        str = str.left(str.length()-1);
        datalist.append(str);
    }

    return datalist;
}

bool DbHandle::updateRecord(const QString& dbName, const QString & table,int no,QVariantMap &data)
{
    return updateRecord(dbName, table, "No='"+QString::number(no)+"'", data);
}

bool DbHandle::updateRecord(const QString& dbName, const QString &table, const QString &filter, const QVariantMap &data)
{
    auto iter = m_dbMap.find(dbName);
    if ( iter == m_dbMap.end() )
        return false;

    QSqlDatabase *pDb = iter.value();
    if ( !pDb->isOpen() )
    {
        qDebug()<<"db: "<<dbName<<" is not open.";
        return false;
    }

    QSqlTableModel tbl(this,*pDb);
    QSqlRecord record;
    tbl.setEditStrategy(QSqlTableModel::OnManualSubmit);
    tbl.setTable(table);
    tbl.setFilter(filter);

    if(tbl.select())
    {
        if(tbl.rowCount()>=1)
        {
            record = tbl.record(0);
            QMapIterator<QString, QVariant> i(data);
            while (i.hasNext()) {
                i.next();
                record.setValue(i.key(),i.value());
            }
            tbl.setRecord(0,record);

            return tbl.submitAll();
        }
        else
            qDebug()<<"tbl.rowCount error.";
    }
    else
        qDebug()<<"select error: "<<tbl.lastError();

    return false;
}

//
bool DbHandle::insertRecord(const QString& dbName, QString table,QVariantMap data)
{
    auto iter = m_dbMap.find(dbName);
    if ( iter == m_dbMap.end() )
        return false;

    QSqlDatabase *pDb = iter.value();
    if ( !pDb->isOpen() )
    {
        qDebug()<<"db: "<<dbName<<" is not open.";
        return false;
    }

    QSqlTableModel tbl(this,*pDb);
    QSqlRecord record;
    tbl.setEditStrategy(QSqlTableModel::OnManualSubmit);
    tbl.setTable(table);

    record = tbl.record(0);
    QMapIterator<QString, QVariant> i(data);
    while (i.hasNext()) {
        i.next();
        record.setValue(i.key(),i.value());
    }
    tbl.insertRecord(-1,record);

    return tbl.submitAll();
}

//执行sql命令语句，只需要返回执行成功或者失败
bool DbHandle::execCmd(const QString &dbName, const QString &sql)
{
    bool bRet = false;
    auto iter = m_dbMap.find(dbName);
    if ( iter == m_dbMap.end() )
        return false;

    QSqlDatabase *pDb = iter.value();
    if ( !pDb->isOpen() )
    {
        qDebug()<<"db: "<<dbName<<" is not open.";
        return false;
    }

    QSqlQuery query(*pDb);
    bRet = query.exec(sql);
//        qDebug()<<"sql cmd:  "<<sql<<", ret = "<<bRet;
    if ( !bRet )
    {
        qDebug()<<query.lastError();
    }

    return bRet;

}

bool DbHandle::deleteRecord(const QString& dbName, QString table,int no)
{
    auto iter = m_dbMap.find(dbName);
    if ( iter == m_dbMap.end() )
        return false;

    QSqlDatabase *pDb = iter.value();
    if ( !pDb->isOpen() )
    {
        qDebug()<<"db: "<<dbName<<" is not open.";
        return false;
    }

    QSqlTableModel tbl(this,*pDb);
    tbl.setEditStrategy(QSqlTableModel::OnManualSubmit);
    tbl.setTable(table);
    tbl.setFilter("No='"+QString::number(no)+"'");
    if(tbl.select())
    {
        if(tbl.rowCount()>=1)
        {
            tbl.removeRow(0);
            return tbl.submitAll();
        }
    }

    return false;
}

bool DbHandle::deleteRecord(const QString& dbName, QString table,QString filter)
{
    auto iter = m_dbMap.find(dbName);
    if ( iter == m_dbMap.end() )
        return false;

    QSqlDatabase *pDb = iter.value();
    if ( !pDb->isOpen() )
    {
        qDebug()<<"db: "<<dbName<<" is not open.";
        return false;
    }

    QSqlTableModel tbl(this,*pDb);
    tbl.setEditStrategy(QSqlTableModel::OnManualSubmit);
    tbl.setTable(table);
    tbl.setFilter(filter);
    if(tbl.select())
    {
        if(tbl.rowCount()>=1)
        {
            tbl.removeRow(0);
            return tbl.submitAll();
        }
    }

    return false;
}
#include <QFile>
bool DbHandle::backupDatabase(const QString &dbName, QString url)
{
    if(QFile::exists(url))
    {
        if(QFile::remove(url))
        {
            return QFile::copy(dbName,url);
        }
    }
    else return QFile::copy(dbName,url);
    return false;
}

bool DbHandle::restoreDatabase(const QString &dbName, QString url)
{
    if(isDbOpen(dbName))
        m_dbMap[dbName]->close();

    bool res = false;
    QString strPath = dbName;
    if(QFile::exists(strPath))
    {
        if(QFile::remove(strPath))
        {
            res = QFile::copy(url,strPath);
        }
    }
    else
        res = QFile::copy(url,strPath);

    if( openDb(dbName) )
        return res;

    return false;
}

