﻿#include <QCoreApplication>
#include "dbhandle.h"
#include "cdatabase.h"
#include <QDebug>

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
using namespace std ;




int main(int argc, char *argv[])
{

    CDataBase dataBase;

    dataBase.init("test.db");

    auto vlist = dataBase.getData("test.db", "select * from port");

    qDebug()<<"QVariantList:  "<<vlist;

    QJsonArray jlist = QJsonArray::fromVariantList(vlist);

    qDebug()<<"Json:  "<<jlist;

    QJsonDocument jdoc;
    jdoc.setArray(jlist);

    qDebug()<<"style json string: "<<jdoc.toJson();

    return 0;
}
