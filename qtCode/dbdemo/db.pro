#QT += core
#QT -= gui
QT += sql network
CONFIG += c++11
CONFIG += console
CONFIG -= app_bundle

TARGET          = db
MOC_DIR         = temp/moc
RCC_DIR         = temp/rcc
UI_DIR          = temp/ui
OBJECTS_DIR     = temp/obj
DESTDIR         = $$PWD/bin
# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

LIBS +=  #-lcryptopp
# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    dbhandle.cpp \
    cdatabase.cpp


HEADERS += \
    dbhandle.h \
    cdatabase.h


INCLUDEPATH += C:\qtwork\boost_1_56\bin\include
INCLUDEPATH += ./jsoncpp


DISTFILES += \
    jsoncpp/src/sconscript \


