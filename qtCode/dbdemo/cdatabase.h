﻿#ifndef CDATABASE_H
#define CDATABASE_H

#include <QObject>
#include "dbhandle.h"
#include <QTimer>
#include <QVariantList>

#define TIME_OUT_TIMES   10 //10分钟没有访问，则将数据库关闭，需要访问的时候再打开

#ifdef WIN32

#define SYS_CONFIG_DB       "./iot_cfg.db"
#define DATA_DB_FILE        "./data/note.db"
#define ALARM_DB_FILE       "./alarm.db"
#define SYS_CONFIG_FILE     "./sysConfig.json"
#define SYS_LOCAL_CONFIG    "./config.json"
#define STATE_DEVICE_FILE   "./device_status"
#define STATE_NETWORK       "./4g_state.json"
#define STATE_WIFI          "./wifi_state.json"
#define IMG_Path            "/usr/local/www/images/three/default2.jpg"
#define IMG_BASE64_Path     "./25D.json"
#define DEVICE_ADDR         "./device_addr.json"
#define ALARM_FILE          "./alarm.json"
#define RTSP_CONFIG         "/work/data/rtsp2webrtc/config.json"

#else

#define SYS_CONFIG_DB     "/work/iot_cfg.db"
#define DATA_DB_FILE      "/work/data/note.db"
#define ALARM_DB_FILE     "/work/data/alarm.db"
#define SYS_CONFIG_FILE   "/work/sysConfig.json"
#define SYS_LOCAL_CONFIG  "/work/config.json"
#define STATE_DEVICE_FILE "/tmp/device_status"
#define STATE_NETWORK     "/tmp/4g_state.json"
#define STATE_WIFI        "/tmp/wifi_state.json"
#define IMG_Path          "/usr/local/www/images/three/default2.jpg"
#define IMG_BASE64_Path   "/work/25D.json"
#define DEVICE_ADDR       "/work/device_addr.json"
#define ALARM_FILE        "/tmp/alarm.json"
#define RTSP_CONFIG       "/work/data/rtsp2webrtc/config.json"

#endif

class CDataBase : public QObject
{
    Q_OBJECT
public:
    explicit CDataBase(QObject *parent = nullptr);
    virtual ~CDataBase();
    void init(const QString &name);

    bool insert(const QString &dbName, QString table, const QVariantList &data);

    bool deleteData(QString table,QString filter);
    bool deleteConfig(QString table,QString filter);

    bool updateConfig(QString table, const QVariantList &filter, const QVariantList &data);
    bool updateData(QString table, const QVariantList &filter, const QVariantList &data);
    bool update(const QString& dbName, QString table, const QVariantList &filter, const QVariantList &data);
    bool update(const QString& dbName, const QString &table, const QString &filter, const QVariantMap &data);

    QVariantList getData(const QString& dbName, QString sql);
    QVariantList getOneData(const QString& dbName, QString sql);
    bool execCmd(const QString &dbName, QString sql);
    QString getValue(const QString& dbName, const QString &key);
    bool setValue(const QString& dbName, const QString &key,const QString &value);

private:


signals:


private:
    DbHandle *m_pDbHandle;
};

#endif // CDATABASE_H
