﻿#ifndef DBHANDLE_H
#define DBHANDLE_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QSqlRecord>
#include <QSqlField>

#define MAXTEMP 1280
#define MINTEMP -640


class DbHandle : public QObject
{
    Q_OBJECT
public:


    Q_INVOKABLE QString getParameter(const QString& dbName, QString name);
    Q_INVOKABLE bool setParameter(const QString &dbName, QString name, QString value);

    Q_INVOKABLE QVariantList getAllBySelect(const QString& dbName, QString sql);
    Q_INVOKABLE QVariantList getOneBySelect(const QString& dbName, QString sql);
    Q_INVOKABLE QByteArray getBlob(const QString& dbName, QString sql);

    Q_INVOKABLE bool updateRecord(const QString& dbName, const QString &table, int no, QVariantMap &data);
    Q_INVOKABLE bool updateRecord(const QString& dbName, const QString &table, const QString &filter, const QVariantMap &data);
    Q_INVOKABLE bool insertRecord(const QString &dbName, QString table, QVariantMap data);
    Q_INVOKABLE bool deleteRecord(const QString &dbName, QString table, int no);
    Q_INVOKABLE bool deleteRecord(const QString &dbName, QString table, QString filter);
    Q_INVOKABLE bool backupDatabase(const QString &dbName, QString url);
    Q_INVOKABLE bool restoreDatabase(const QString &dbName, QString url);
    //Q_INVOKABLE bool compareReport(QString reportName);
    static DbHandle* getInstance();
    bool creatConnection(const QString &ip, const QString &port, const QString &username, const QString &passwd, const QString &dbName);
    bool creatConnection(const QString& dbName);
    void closeConnection(const QString& dbName);
    bool execCmd(const QString &dbName, const QString &sql);

    bool openDb(const QString &dbName);
    bool isDbOpen(const QString &dbName);

signals:
    void noticeMsg(const QString&);

private:

    QMap<QString, QSqlDatabase*>m_dbMap;

protected:
    Q_DISABLE_COPY(DbHandle)
    explicit DbHandle(QObject *parent = nullptr);
    ~DbHandle();
 private:

    union{
        short v;
        unsigned char hl[2];
    }ByteConvertor;
};

#endif // DBHANDLE_H
