﻿#include "cmainscreen.h"
#include "ui_cmainscreen.h"
#include <QFileDialog>
#include <QDebug>
#include <QLabel>

CMainScreen::CMainScreen(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CMainScreen)
{
    ui->setupUi(this);


    m_pMovie = new QMovie;
    ui->gifLabel->setMovie(m_pMovie);

    m_pGif = NULL;
}

CMainScreen::~CMainScreen()
{
    delete ui;
}

void CMainScreen::on_btnAdd_clicked()
{
    QFileDialog fileDialog(this);

    //设置窗口的标题
    fileDialog.setWindowTitle("请选择图片文件");
    fileDialog.setNameFilter("图片文件(*.jpg *.png)"); //设置一个过滤器
    fileDialog.setFileMode(QFileDialog::ExistingFiles);
//    fileDialog.setAcceptMode(QFileDialog::AcceptSave); //保存文件
    if (fileDialog.exec() == QDialog::Accepted)
    {

    //strPathList  返回值是一个list，如果是单个文件选择的话，只要取出第一个来就行了。
        m_fileList = fileDialog.selectedFiles();
    }

    qDebug()<<m_fileList;
    QLabel *pLabel = NULL;
    QImage *pImg = NULL;
    for ( int i = 0; i < m_fileList.size(); i++ )
    {
        pLabel = new QLabel;
        pImg = new QImage;
        pImg->load(m_fileList.at(i)); //将图像资源载入对象img，注意路径，可点进图片右键复制路径
        pLabel->setPixmap(QPixmap::fromImage(*pImg)); //将图片放入label，使用setPixmap,注意指针*img

        ui->image->addWidget(pLabel, i, 0);

    }

    m_w = pImg->width();
    m_h = pImg->height();

}

void CMainScreen::on_btnCreat_clicked()
{
    Gif gif;
    Gif::GifWriter *gifWriter = new Gif::GifWriter;

    int fs = ui->frameTime->text().toInt()/10;
    bool ok = gif.GifBegin(gifWriter, "tmp.gif", m_w, m_h, fs);
    if(!ok)
    {
        delete gifWriter;
        gifWriter = 0;
        return ;
    }

    for ( int i = 0; i < m_fileList.size(); i++ )
    {
        QImage image;
        image.load(m_fileList.at(i)); //将图像资源载入对象img，注意路径，可点进图片右键复制路径
        image = image.convertToFormat(QImage::Format_RGBA8888);

        gif.GifWriteFrame(gifWriter, image.bits(), image.width(), image.height(), fs);

    }

    gif.GifEnd(gifWriter);

    m_pMovie->stop();
    m_pMovie->setFileName("tmp.gif");
    m_pMovie->setScaledSize(QSize(m_w, m_h));

//    ui->gifLabel->setPixmap(QPixmap::fromImage(imag));
    ui->gifLabel->resize(m_w, m_h);

    m_pMovie->start();
}

void CMainScreen::on_btnrec_clicked()
{
        if ( m_pGif != NULL )
            return;

        m_pGif = new GitWidget();
        m_pGif->show();
}
