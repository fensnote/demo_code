﻿#include "gitwidget.h"
#include <QDebug>

GitWidget::GitWidget(QWidget *parent) : QDialog(parent)
{
    setObjectName("GifWidget");
    resize(800, 600);
    setSizeGripEnabled(true);

    QVBoxLayout *verticalLayout = new QVBoxLayout(this);
    verticalLayout->setSpacing(0);
    verticalLayout->setContentsMargins(0,0,0,0);

    widgetTop = new QWidget(this);
    widgetTop->setMinimumSize(0, 35);
    widgetTop->setMaximumSize(8000, 35);

    QHBoxLayout *layoutTop = new QHBoxLayout(widgetTop);
    layoutTop->setSpacing(0);
    layoutTop->setContentsMargins(0,0,0,0);

    QPushButton *btnIcon = new QPushButton(widgetTop);
    QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    sizePolicy.setHeightForWidth(btnIcon->sizePolicy().hasHeightForWidth());
    btnIcon->setSizePolicy(sizePolicy);
    btnIcon->setMinimumSize(35, 0);
    btnIcon->setFlat(true);
    layoutTop->addWidget(btnIcon);

    QLabel *labTitle = new QLabel(widgetTop);
    layoutTop->addWidget(labTitle);

    QSpacerItem *horizontalSpacer = new QSpacerItem(87, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    layoutTop->addItem(horizontalSpacer);

    QPushButton *btnClose = new QPushButton(widgetTop);
    sizePolicy.setHeightForWidth(btnClose->sizePolicy().hasHeightForWidth());
    btnClose->setSizePolicy(sizePolicy);
    btnClose->setMinimumSize(35, 0);
    btnClose->setFocusPolicy(Qt::NoFocus);
    btnClose->setFlat(true);
//    btnClose->setIcon(this->style().;

    layoutTop->addWidget(btnClose);
    verticalLayout->addWidget(widgetTop);

    widgetMain = new QWidget(this);
    QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Expanding);
    sizePolicy1.setHorizontalStretch(0);
    sizePolicy1.setVerticalStretch(0);
    sizePolicy1.setHeightForWidth(widgetMain->sizePolicy().hasHeightForWidth());
    widgetMain->setSizePolicy(sizePolicy1);
    verticalLayout->addWidget(widgetMain);

    widgetBottom = new QWidget(this);
    widgetBottom->setMinimumSize(0, 45);
    widgetBottom->setMaximumSize(8000, 45);

    QHBoxLayout *layoutBottom = new QHBoxLayout(widgetBottom);
    layoutBottom->setSpacing(6);
    layoutBottom->setContentsMargins(9,9,-1,-1);

    QLabel *labFps = new QLabel(widgetBottom);
    layoutBottom->addWidget(labFps);

    txtFps = new QLineEdit(widgetBottom);
    txtFps->setMaximumSize(50, 12580);
    txtFps->setAlignment(Qt::AlignCenter);
    layoutBottom->addWidget(txtFps);

    QLabel *labWidth = new QLabel(widgetBottom);
    layoutBottom->addWidget(labWidth);

    txtWidth = new QLineEdit(widgetBottom);
    txtWidth->setEnabled(true);
    txtWidth->setMaximumSize(50, 12580);
    txtWidth->setAlignment(Qt::AlignCenter);
    layoutBottom->addWidget(txtWidth);

    QLabel *labHeight = new QLabel(widgetBottom);
    layoutBottom->addWidget(labHeight);

    txtHeight = new QLineEdit(widgetBottom);
    txtHeight->setMaximumSize(50, 12580);
    txtHeight->setAlignment(Qt::AlignCenter);
    layoutBottom->addWidget(txtHeight);

    labStatus = new QLabel(widgetBottom);
    QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Preferred);
    sizePolicy2.setHorizontalStretch(0);
    sizePolicy2.setHeightForWidth(labStatus->sizePolicy().hasHeightForWidth());
    sizePolicy2.setVerticalStretch(0);
    labStatus->setSizePolicy(sizePolicy2);
    layoutBottom->addWidget(labStatus);

    btnStart = new QPushButton(widgetBottom);
    sizePolicy.setHeightForWidth(btnStart->sizePolicy().hasHeightForWidth());
    btnStart->setSizePolicy(sizePolicy);
    layoutBottom->addWidget(btnStart);
    verticalLayout->addWidget(widgetBottom);

    labTitle->setText("GIF录屏工具");
    labFps->setText("帧率");
    labWidth->setText("宽度");
    labHeight->setText("高度");
    btnStart->setText("开始");
    setWindowTitle(labTitle->text());

    connect(btnClose, &QPushButton::clicked, this, &GitWidget::closeAll);
    connect(btnStart, &QPushButton::clicked, this, &GitWidget::record);
    connect(txtWidth, &QLineEdit::editingFinished, this, &GitWidget::resizeForm);
    connect(txtHeight, &QLineEdit::editingFinished, this, &GitWidget::resizeForm);

    borderWidth = 3;
    bgColor = QColor(34, 163, 169);

    fps = 10;
    txtFps->setText(QString::number(fps));
    gifWriter = 0;

    timer = new QTimer(this);
    timer->setInterval(100);
    connect(timer, &QTimer::timeout, this, &GitWidget::saveImage);

    setAttribute(Qt::WA_TranslucentBackground);
    setWindowFlags(Qt::FramelessWindowHint);
    installEventFilter(this);

    btnClose->setObjectName("btnClose");
    labTitle->setObjectName("labTitle");
    labStatus->setObjectName("labStatus");

    QStringList qss;
    qss.append("QLabel{color:#ffffff;}");
    qss.append("#btnClose,#btnIcon{border:none;border-radius:0px;}");
    qss.append("#btnClose:hover{background-color:#ff0000;}");
    qss.append("#btnClose{border-top-right-radius:5px;}");
    qss.append("#labTitle{font:bold 16px;}");
    qss.append("#labStatus{font:15px;}");
    setStyleSheet(qss.join(""));

    txtWidth->setEnabled(true);
    txtHeight->setEnabled(true);
}

void GitWidget::closeAll()
{
    close();
}

void GitWidget::record()
{
    if(btnStart->text() == "开始")
    {
        if(gifWriter != 0)
        {
            delete gifWriter;
            gifWriter = 0;
        }

        fileName = QFileDialog::getSaveFileName(this, "保存", qApp->applicationDirPath() + "/", "gif图片(*.gif)");
        if(fileName.isEmpty())
            return ;

        int width = txtWidth->text().toInt();
        int height = txtHeight->text().toInt();
        fps = txtFps->text().toInt();

        gifWriter = new Gif::GifWriter;
        bool ok = gif.GifBegin(gifWriter, fileName.toLocal8Bit().data(), width, height, fps);
        if(!ok)
        {
            delete gifWriter;
            gifWriter = 0;
            return ;
        }

        count = 0;
        labStatus->setText("开始录制...");
        btnStart->setText("停止");

        timer->setInterval(1000/fps);
        QTimer::singleShot(1000, timer, SLOT(start()));
    }
    else
    {
        timer->stop();
        gif.GifEnd(gifWriter);

        delete gifWriter;
        gifWriter = 0;

        labStatus->setText(QString("录制完成 共 %1 帧").arg(count));
        btnStart->setText("开始");
        QDesktopServices::openUrl(QUrl(fileName));
    }
}

void GitWidget::resizeForm()
{
    int width = txtWidth->text().toInt();
    int height = txtHeight->text().toInt();
    resize(width, height+widgetTop->height()+widgetBottom->height());
}

bool GitWidget::eventFilter(QObject *watched, QEvent *e)
{
    static QPoint mousePoint;
    static bool mousePressed = false;

    QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(e);
    if(mouseEvent->type() == QEvent::MouseButtonPress)
    {
        if(mouseEvent->button() == Qt::LeftButton)
        {
            mousePressed = true;
            mousePoint = mouseEvent->globalPos() - this->pos();
            return true;
        }
    }
    else if(mouseEvent->type() == QEvent::MouseButtonRelease)
    {
        mousePressed = false;
        return true;
    }
    else if(mouseEvent->type() == QEvent::MouseMove)
    {
        if(mousePressed)
        {
            this->move(mouseEvent->globalPos() - mousePoint);
            return true;
        }
    }

    return QWidget::eventFilter(watched, e);
}

void GitWidget::resizeEvent(QResizeEvent *e)
{
    txtWidth->setText(QString::number(widgetMain->width()));
    txtHeight->setText(QString::number(widgetMain->height()));
    QDialog::resizeEvent(e);
}

void GitWidget::paintEvent(QPaintEvent *)
{
    int width = txtWidth->text().toInt();
    int height = txtHeight->text().toInt();

    rectGif = QRect(borderWidth, widgetTop->height(), width-(borderWidth*2), height);

    QPainter painter(this);
    painter.setPen(Qt::NoPen);
    painter.setBrush(bgColor);
    painter.drawRoundedRect(this->rect(), 5, 5);
    painter.setCompositionMode(QPainter::CompositionMode_Clear);
    painter.fillRect(rectGif, Qt::SolidPattern);
}

void GitWidget::saveImage()
{
    if(gifWriter == 0)
    {
        return ;
    }

    QScreen *screen = QApplication::primaryScreen();
    QPixmap pixmap = screen->grabWindow(0, x()+rectGif.x(), y()+rectGif.y(), rectGif.width(), rectGif.height());
    QImage image = pixmap.toImage().convertToFormat(QImage::Format_RGBA8888);

    gif.GifWriteFrame(gifWriter, image.bits(), rectGif.width(), rectGif.height(), fps);
    count++;
    labStatus->setText(QString("正在录制 第 %1 帧").arg(count));
}















