﻿#ifndef CMAINSCREEN_H
#define CMAINSCREEN_H

#include <QWidget>
#include <QMovie>
#include "gitwidget.h"

namespace Ui {
class CMainScreen;
}

class CMainScreen : public QWidget
{
    Q_OBJECT

public:
    explicit CMainScreen(QWidget *parent = 0);
    ~CMainScreen();

private slots:
    void on_btnAdd_clicked();

    void on_btnCreat_clicked();

    void on_btnrec_clicked();

private:
    Ui::CMainScreen *ui;
    QStringList m_fileList;
    QMovie *m_pMovie;

    int m_w,m_h;
    GitWidget *m_pGif;
};

#endif // CMAINSCREEN_H
