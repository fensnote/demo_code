﻿#ifndef CTESTAUDIOPLAY_H
#define CTESTAUDIOPLAY_H

#include <QWidget>
#include <QMediaPlayer>

namespace Ui {
class CTestAudioPlay;
}

class CTestAudioPlay : public QWidget
{
    Q_OBJECT

public:
    explicit CTestAudioPlay(QWidget *parent = 0);
    ~CTestAudioPlay();

private slots:
    void on_btnPlay_clicked();

    void on_btnStop_clicked();

    void on_btnSelect_clicked();

private:
    Ui::CTestAudioPlay *ui;
    QMediaPlayer *m_pPlayer;
    bool isPlay = false;
};

#endif // CTESTAUDIOPLAY_H
