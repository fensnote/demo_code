﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPrintPreviewDialog>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();
    void preview(QPrinter *printer);

    void on_btnCreatPdf_clicked();

    void on_btnReport_clicked();

    void on_tableWidget_cellClicked(int row, int column);

    void on_btnPrintTable_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
