﻿#ifndef QPRINTERMANAGER_H
#define QPRINTERMANAGER_H
//#include "libmanager_global.h"
#include <QObject>
#include <QAbstractItemModel>
#include <QTextStream>
#include <QPrinter>
/*******************************
* class QPrinterManager
* 此类为打印模块，实现数据表格的组织
* 和打印业务。
********************************/
class  QPrinterManager : public QObject
{
    Q_OBJECT
public:
    explicit QPrinterManager(QObject *parent = 0);
    ~QPrinterManager(){}
    void setCharSet(const QString &set = QString("utf-8")){ m_charSet = set; }
    void dataBegin();
    void dataEnd(){ m_out << QString("</body></html>"); }
    void insertTitle(const QString &title);             // 添加主标题（两行：第一行居中显示文字，第二行居右显示时间）
    void insertTitle2(const QString &title2);           // 添加次级标题
    void insertSeperator(){ m_out << QString("<br>\n"); } // 空出一行
    void tableBegin(const QStringList &head);       // 不支持嵌套TABLE
    void tableEnd(){ m_out << QString("</table></div>\n"); }
    void insert2TableRow(const QStringList &rowData);
    void printWithPreview();// 带预览
    void printDirect();     // 直接打印，不带预览
private:
    void resetState();
private slots:
    void getPreviewData(QPrinter *printer);
private:
    QString m_charSet;
    QString m_data;
    QTextStream m_out;
};
#endif // QPRINTERMANAGER_H
