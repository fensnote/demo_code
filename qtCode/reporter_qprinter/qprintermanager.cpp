﻿#include "QPrinterManager.h"
#include <QTextEdit>
#include <QPrintPreviewDialog>
#include <QPrintDialog>
#include <QTextDocument>
#include <QPrinter>
#include <QDateTime>
#include <QDebug>
#include <QTextCursor>
#include <QTextTable>
#include <QTextTableFormat>

QPrinterManager::QPrinterManager(QObject *parent) : QObject(parent), m_charSet("utf-8")
{
    m_out.setString(&m_data);
}
void QPrinterManager::dataBegin()
{
    m_out << QString("<html>")
        << QString("<head>")
        << QString("<meta content=\"text/html; charset=%1\">").arg(m_charSet)
        << QString("<meta name=Generator content=\"Microsoft Word 12 (filtered)\">")
        << QString("</head>")
        << QString("<body>");
}
void QPrinterManager::insertTitle(const QString &title)
{
    m_out << QString("<p style='text-align:center'><span style='font-size:16.0pt;font-family:宋体'>%1</span></p>\n")
        .arg(title)
        << QString("<p style='text-align:right'><span style='font-size:12.0pt;font-family:宋体'>%1</span></p>\n")
        .arg(QDateTime::currentDateTime().toString("yyyy-MM-dd,HH:mm:ss"));
}
void QPrinterManager::insertTitle2(const QString &title2)
{
    m_out << QString("<p style='text-align:left'><span style='font-size:15.0pt;font-family:宋体'><b>-%1</b></span></p>\n")
        .arg(title2);
}
void QPrinterManager::tableBegin(const QStringList &head)
{
    // width='100%' height='1000px'
    m_out << QString("<div align='center' style='width:1300px; height=1000px; margin-left: 30px; margin-top: -20px'>")
        << QString("<table class=print border=1 cellspacing=0 cellpadding=0 style='border-collapse:collapse;width:1300px; height=1000px;'>\n")
        << QString("<tr >\n");

    m_out << QString("<td colspan=2><p><span ><p align='center'><span align='center' ></span></p></td>\n");
    for (int i = 0; i < head.count(); ++i)
    {
        //style='border:solid windowtext 1.0pt'
        m_out << QString("<td nowrap='nowarp' ><p align='center'><span align='center' ><b>%1</b></span></p></td>\n")
            .arg(head.at(i));
    }
    m_out << "</tr>\n";
}
void QPrinterManager::insert2TableRow(const QStringList &rowData)
{
    m_out << QString("<tr>\n");
    m_out << QString("<td colspan=2 rowspan=3><p><span ><p align='center'><span align='center' ></span></p></td>\n");
    for (int i = 0; i < rowData.count(); ++i)
    {
        //style='font-size:10.0pt;font-family:宋体' style='border:solid windowtext 1.0pt'
        m_out << QString("<td><p><span ><p align='center'><span align='center' >%1</span></p></td>\n")
            .arg(rowData.at(i));
    }
    m_out << QString("</tr>\n");
}
void QPrinterManager::printWithPreview()
{
    QPrinter printer(QPrinter::ScreenResolution);
    printer.setPageSize(QPrinter::A4);
    printer.setOrientation(QPrinter::Landscape); //打印方向

    QPrintPreviewDialog preview(&printer);
    connect(&preview, SIGNAL(paintRequested(QPrinter*)), this, SLOT(getPreviewData(QPrinter*)));
    preview.setWindowState(Qt::WindowMaximized);
    preview.exec();
    resetState();
}
void QPrinterManager::printDirect()
{
    QPrinter printer(QPrinter::ScreenResolution);
    printer.setPageSize(QPrinter::A4);
    QPrintDialog printDialog(&printer);
    printer.setOrientation(QPrinter::Portrait); //打印方向

    if (printDialog.exec() == QDialog::Accepted)
    {
        getPreviewData(&printer);
    }
    resetState();
}
void QPrinterManager::resetState()
{
    m_data.clear();
    m_charSet = "utf-8";
}
void QPrinterManager::getPreviewData(QPrinter *printer)
{
    qDebug()<<m_data;
    QTextDocument document;
    document.setHtml(m_data);

    document.print(printer);
}
