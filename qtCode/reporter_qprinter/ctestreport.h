﻿#ifndef CTESTREPORT_H
#define CTESTREPORT_H

#include <QObject>
#include <QAbstractItemModel>
#include <QPrinter>
#include <QVariantMap>
#include <QVariantList>

class CTestReport:public QObject
{
    Q_OBJECT
public:
    explicit CTestReport(QObject *parent = nullptr);
    virtual ~CTestReport();



    void printWithPreview();
    void printDirect();
    void printOnePage(QPainter *painter, int no);

private:
    void drawTable(QPainter *painter, int no);
signals:

public slots:

private slots:
    void printDocument(QPrinter *printer);

private:

    int m_startx;    //整体表格在A4纸中的起始x坐标
    int m_starty;    //整体表格在A4纸中的起始y坐标
    int m_width;     //整体表格的宽
    int m_height;    //整体表格的高

    int m_titleHeight;    //标题栏的高

    int m_colNum ;   //表格的总列数
    int m_rowNum ;   //表格的总行数
    int m_rowHeight; //单元表格高
    int m_colWidth;  //单元表格宽

};


#endif // CTESTREPORT_H
