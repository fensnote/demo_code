﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QPrinter>
#include <QDebug>
#include <QPainter>
#include "printapi.h"
#include "qprintermanager.h"
#include <QSharedPointer>
#include "ctestreport.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tableWidget->setColumnCount(5);
    ui->tableWidget->setRowCount(10);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QPrinter printer;
    printer.setPageSize(QPrinter::A4);
    printer.setOrientation(QPrinter::Landscape); //有QPrinter::Portrait(纵向)和QPrinter::Landscape(横向)
    QPrintPreviewDialog previewDialog(&printer,this);
    connect(&previewDialog,&QPrintPreviewDialog::paintRequested,this,&MainWindow::preview);
    previewDialog.exec(); //打印对话框显示，paintRequest触发
}

void MainWindow::preview(QPrinter *printer)
{
    ui->textEdit->print(printer);
}

void MainWindow::on_btnCreatPdf_clicked()
{
    QPrinter printer;
    QFileDialog fileDialog;
    QString str = QFileDialog::getSaveFileName(this,tr("Save Text"),"/",tr("Text Files (*.pdf)"));
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPageSize(QPrinter::A4);
    printer.setOutputFileName(str);
    ui->textEdit->print(&printer);
}

void MainWindow::on_btnReport_clicked()
{
    CTestReport reporter;

    reporter.printWithPreview();

}

void MainWindow::on_tableWidget_cellClicked(int row, int column)
{
    qDebug()<<"row: "<<row<<", col: "<<column;

}

void MainWindow::on_btnPrintTable_clicked()
{
    QString type = "所有告警记录";
    int col = 22;
    QList<QString> columnNames;
    QList<int> columnWidths;
    QStringList content;
    QString rowContent;
    for (int i = 0; i < col; i++)
    {
        columnNames.append(QString("第%1列").arg(i));
        columnWidths.append(1100 / col);
    }

    content.append("值1-1;值1-2;值1-3;值1-4;值1-5;");
    content.append("值2-1;值2-2;值2-3;值2-4;值2-5");
    content.append("值3-1;值3-2;值3-3;值3-4;值3-5");
    content.append("值4-1;值4-2;值4-3;值4-4;值4-5");
    content.append("值5-1;值5-2;值5-3;值5-4;值5-5");

    PrintAPI::Instance()->PrintA4(type, "", columnNames, columnWidths, content, true, false);
}




