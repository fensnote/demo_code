﻿#ifndef CPICTURETABLE_H
#define CPICTURETABLE_H

#include <QObject>

#include <QPushButton>
#include <QPainter>
#include <QPixmap>
#include <QPicture>

class CPictureTable : public QObject
{
    Q_OBJECT
public:
    explicit CPictureTable(QObject *parent = nullptr);
    ~CPictureTable();

signals:

public slots:
};

#endif // CPICTURETABLE_H
