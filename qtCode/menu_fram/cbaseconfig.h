#ifndef CBASECONFIG_H
#define CBASECONFIG_H

/************************************************************************
 * 名称：上导航菜单栏界面
 * 上边是导航按键，下面是对应的界面
 * 作者：fens
*************************************************************************/

#include <QWidget>
#include <QToolButton>
#include <QMap>

namespace Ui {
class CBaseConfig;
}

class CBaseConfig : public QWidget
{
    Q_OBJECT

public:
    explicit CBaseConfig(QWidget *parent = nullptr);
    ~CBaseConfig();
    void setMenuMiniWidth(int minw);
    void setMenuMiniHeight(int minh);
    void setMenuMaxWidth(int maxw);
    void setMenuMaxHeight(int maxh);

    //用于设置按键属性,固定大小/缩放
    void setMenuBtnSizePolicy(QSizePolicy val);
    //初始化生成菜单按键和对应界面
    void initMenu(QVector<QPair<QString, QWidget*> >& listItem);

signals:
    void clicked(int , bool ); //按键序号，从0开始， 状态-true选中；false-未选中

private slots:
    void onToolButtonClicked();

private:
    Ui::CBaseConfig *ui;

    QString m_bakebtnName; //备份当前显示的界面按键
    QVector<QToolButton*>m_btnMap;
    QMap<QString, QWidget*>m_widgetsMap;
    QSizePolicy m_btnQSizePolicy;
};

#endif // CBASECONFIG_H
