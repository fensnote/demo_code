#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "cbaseconfig.h"
#include <QLabel>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    init();

    //设置菜单键样式，实际应用时，建议写在单独的qss文件中
    QStringList qss;
    qss.append("QToolButton{color:rgb(250, 250,250);font:14px;min-width:72px;min-height:37px;background-color: qlineargradient(spread:pad,x1:0,y1:0,x2:0,y2:1,stop:0 #0931B4,stop:1 #050A51);border:1px;border-right-color: rgb(20, 20, 20);border-left-color: rgb(20, 20, 20);/*border-radius控制圆角大小*/	}");
    qss.append("QToolButton:pressed{background-color: qlineargradient(spread:pad,x1:0,y1:0,x2:0,y2:1,stop:0 #0931B4,stop:1 #050A51);}");
    qss.append("QToolButton:checked{background-color: qlineargradient(spread:pad,x1:0,y1:0,x2:0,y2:1,stop:0 #420064,stop:1 #0D22DE);}");

    this->setStyleSheet(qss.join(""));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::init()
{
    int heigth = 76;
    CBaseConfig *baseConfig = new CBaseConfig;
    baseConfig->setMenuMiniHeight(heigth);
    baseConfig->setMenuMaxHeight(heigth);


    QVector<QPair<QString, QWidget*> > listItem;
    //基本设置
    listItem.clear();

    QWidget *pWidget;
    QGridLayout *playout ;
    QLabel *lab;

    for (int i = 1; i <= 7; i++)
    {
        pWidget = new QWidget;
        playout = new QGridLayout;
        lab = new QLabel;
        lab->setText(QString("菜单%1").arg(i));
        lab->setAlignment(Qt::AlignCenter);
        lab->setStyleSheet("font: 48px");

        playout->addWidget(lab);
        pWidget->setLayout(playout);

        listItem.push_back(qMakePair(QString("菜单%1").arg(i),pWidget));
    }

    connect(baseConfig, &CBaseConfig::clicked, this, [this](int no, bool flag){
        if (flag )
            qDebug()<<"no: "<<no<<" enter screen.";
        else
            qDebug()<<"no: "<<no<<" leave screen.";
    });

    baseConfig->initMenu(listItem);
    ui->scrollArea->setWidget(baseConfig);


}
