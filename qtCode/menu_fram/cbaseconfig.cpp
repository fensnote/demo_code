#include "cbaseconfig.h"
#include "ui_cbaseconfig.h"
#include <QDebug>

CBaseConfig::CBaseConfig(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CBaseConfig)
{
    ui->setupUi(this);
    this->setProperty("form", true);

    this->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);

//    m_btnQSizePolicy = QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    m_btnQSizePolicy = QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
}

CBaseConfig::~CBaseConfig()
{
    delete ui;
}

void CBaseConfig::setMenuMiniHeight(int minh)
{
    ui->widgetSetMenu->setMinimumWidth(minh);
}

void CBaseConfig::setMenuMiniWidth(int minw)
{
    ui->widgetSetMenu->setMinimumWidth(minw);
}

void CBaseConfig::setMenuMaxHeight(int maxh)
{
    ui->widgetSetMenu->setMaximumHeight(maxh);
}

void CBaseConfig::setMenuMaxWidth(int maxw)
{
    ui->widgetSetMenu->setMaximumWidth(maxw);
}

void CBaseConfig::setMenuBtnSizePolicy(QSizePolicy val)
{
    m_btnQSizePolicy = val;
}

//根据传入的菜单名称和QWidget界面来生成菜单界面
void CBaseConfig::initMenu(QVector<QPair<QString, QWidget*> >& listItem)
{
    m_btnMap.clear();
    m_widgetsMap.clear();

    QToolButton *ptbn = NULL;

    for (int i = 0; i < listItem.count(); i++)
    {
        ptbn = new QToolButton;
        ptbn->setObjectName(QString("tbSetMenu%1").arg(i));
        ptbn->setText(listItem.at(i).first);
        ptbn->setSizePolicy(m_btnQSizePolicy);
        ptbn->setCheckable(true);
        connect(ptbn, &QToolButton::clicked, this, &CBaseConfig::onToolButtonClicked);

        ui->horizontalLayout->addWidget(ptbn);
        ui->mainLayout->addWidget(listItem.at(i).second);
        listItem.at(i).second->hide();
        m_btnMap.push_back(ptbn);
        m_widgetsMap[listItem.at(i).first] = listItem.at(i).second;
    }

    m_btnMap.at(0)->setChecked(true);
    m_bakebtnName = listItem.at(0).first;
    listItem.at(0).second->show();
}

void CBaseConfig::onToolButtonClicked()
{
    QToolButton *b = (QToolButton *)sender();
    QString name = b->text();

    for ( int i = 0; i < m_btnMap.size(); i++ )
    {
        if (m_btnMap.at(i) == b)
        {
            m_btnMap.at(i)->setChecked(true);
            emit clicked(i, true);
        }
        else
        {
            if( m_btnMap.at(i)->isChecked() )
                emit clicked(i, false);
            m_btnMap.at(i)->setChecked(false);
        }
    }

//     qDebug()<<"in CBaseConfig::onToolButtonClicked, button "<<name<<", checked!"<<"last: "<<m_bakebtnName;

    m_widgetsMap[m_bakebtnName]->hide();
    m_widgetsMap[name]->show();
    m_bakebtnName = name;

}
