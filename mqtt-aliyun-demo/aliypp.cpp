#include "mosquittopp.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>

extern int aiotMqttSign(const char *productKey, const char *deviceName, const char *deviceSecret, 
                     	char clientId[150], char username[65], char password[65]);

using namespace std;

#define EXAMPLE_PRODUCT_KEY			"hj1skja****" //更换为自己的key
#define EXAMPLE_DEVICE_NAME			"DEV***"	  //更换为自己的设备名字
#define EXAMPLE_DEVICE_SECRET       "7bb8c2cfb6908c29d3922aa0********" //更换为自己的机密


class MyMqtt : public mosqpp::mosquittopp
{
public:
    MyMqtt(const char *id, const char *host, int port, const char *username, const char *password) : mosquittopp(id)
    {
        mosqpp::lib_init(); // 初始化mosquitto库
        username_pw_set(username, password); // 设置用户名和密码
        connect(host, port, 60); // 连接到MQTT服务器
    }

    ~MyMqtt()
    {
        disconnect(); // 断开连接
        mosqpp::lib_cleanup(); // 清理mosquitto库
    }

    void on_connect(int rc)
    {
        if (rc == 0)
        {
            std::cout << "连接成功" << std::endl;
            subscribe(NULL, "/sys/hj1skja****/DEV***/thing/event/property/post_reply", 0); // 订阅主题
            subscribe(NULL, "/sys/hj1skja****/DEV***//thing/event/property/set", 0); // 订阅主题
        }
        else
        {
            std::cout << "连接失败" << std::endl;
        }
    }

    void on_message(const struct mosquitto_message *message)
    {
        std::cout << "收到消息：" << (char *)message->payload << std::endl;
    }
};

int main(int argc, char *argv[])
{
    const char *mqtt_host = "hj1skja****.iot-as-mqtt.cn-shanghai.aliyuncs.com";
    int mqtt_port = 1883;


    char clientId[256] = {0};
	char username[65] = {0};
	char password[65] = {0};

	if (aiotMqttSign(EXAMPLE_PRODUCT_KEY, EXAMPLE_DEVICE_NAME, EXAMPLE_DEVICE_SECRET, clientId, username, password) < 0) {
		printf("aiotMqttSign error\n");
		return -1;
	}

    printf("clientId: %s\n", clientId);
    printf("username: %s\n", username);
    printf("password: %s\n", password);
    MyMqtt mqtt(clientId, mqtt_host, mqtt_port, username, password);
    mqtt.loop_start(); // 开始循环

    string msg="{\"params\":{\"CurrentTemperature\":27.37,\"CurrentHumidity\":56.8,\"version\":\"ver1.0.1\",\"GeoLocation\":{\"Longitude\":113.987817,\"Latitude\":34.987895,\"Altitude\":123.1,\"CoordinateSystem\":1}}}";


    while (1)
    {
        // 发布消息
        mqtt.publish(NULL, "/sys/hj1skja****/DEV***//thing/event/property/post", msg.size(), msg.c_str());
        sleep(5);
    }
    mqtt.loop_stop(); // 停止循环
    return 0;
}