@[toc]

# 零散Demo代码

## 介绍

>平时写的一些示例代码
>基本框架，封装，自定义控件等

## Qt代码

### Qxlsx报表测试代码

/demo_code/qtCode/testQtxlsx

<img src="/picture/1.png" alt="Qtxlsx" style="zoom:50%;" />

### QPainter+QPrinter报表测试代码

/demo_code/qtCode/reporter_qprinter

<img src="/picture/2.png" alt="效果" style="zoom:50%;" />

不同电脑显示DPI可能会不同，这里可以通过这个函数来设置DPI：

printer->setResolution(96); //设置DPI，必须放在begin前面才能生效



### tcpClient tcp客户端demo

demo_code\qtCode

<img src="picture/tcpClient.png" alt="效果" style="zoom:50%;" />

### 百度地图调用demo

qtCode/gps-map
<img src="https://imgconvert.csdnimg.cn/aHR0cHM6Ly9pbWctYmxvZy5jc2RuLm5ldC8yMDE4MDMxOTE4MDg0Njc3Mj93YXRlcm1hcmsvMi90ZXh0L0x5OWliRzluTG1OelpHNHVibVYwTDNkMWNYVmhibDh4TWpNdy9mb250LzVhNkw1TDJUL2ZvbnRzaXplLzQwMC9maWxsL0kwSkJRa0ZDTUE9PS9kaXNzb2x2ZS83MA?x-oss-process=image/format,png
" alt="效果" style="zoom:80%;" />

### 9宫格图案手势滑动解锁demo

qtCode/lock

<img src="picture/lock.png" alt="效果" style="zoom:80%;" />



### 多菜单界面框架demo

<img src="picture/fram.gif" alt="效果" style="zoom:50%;" />

qtCode/menu_fram

### Qt界面显示opencv视频demo

qtCode/opencv_video

### Qt音频播放

qtCode/qtPlayMp3

可以用与播放wav、MP3等常见格式，功能很强大，很好用；

### QsLog开源日志

demo_code\qtCode\qsLog

轻量级开源qt日志，支持多线程；引用方便。

### qt实现的gif录屏与gif图片合成
demo_code\qtCode\gif_widget

qt实现gif的录制与合成，基础代码是在码云上找的

### QTableView 过滤表格内容
qtCode/filter
使用QSortFilterProxyModel，通过输入关键字来过滤显示表格内容。

## CPP代码

### cpp消息处理框架

cpp/dataProc

<img src="picture/3.png" alt="效果" style="zoom:50%;" />

### C++11多线程demo代码

[cpp\test_fram](cpp\test_fram)

### jsoncpp使用demo

cpp/jsoncpp

### 文件加crc16校验

cpp/file_crc16 

### cpp日志spdlog

[cpp\spdlog](cpp\spdlog)
<img src="picture/日志.png" alt="效果" style="zoom:50%;" />

## Linux应用代码demo

### Linux/fifo demo

Linux/fifo_no_name

### 消息队列

Linux/linux_msg

### 共享内存

Linux/share_mem

### Unix域套接字

Linux/unix_socket


### Linux下一些开源工具代码
Linux/tool


## C代码

### 函数指针-注册回调框架demo

c/test_func_point

### 短信测试代码
实现短信的发送与接收；
短信代码里需要注意的是中文编码的转换；
短信里使用的是PDU编码,汉字是UCS2编码方式，如果代码使用的是UTF8编码，
则需要将UTF8转换为Unicode；

### onvif协议demo代码-c语言版本

demo_code/c/onvif_test

用于搜索网络摄像，获取RTSP播放URL以及PTZ控制等



### mqtt接入阿里云demo代码

mqtt-aliyun-demo
基于mosquito实现阿里云接入。

### C调用C++库接口
c_call_cpp_sample