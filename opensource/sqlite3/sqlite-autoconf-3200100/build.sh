#!/bin/sh
echo $#
if [ $# -eq 0 ] 
then

	echo "usage: $0 <ARCH=arm-hisi-xxxx>"
	exit
fi

dir=`pwd`
ARCH=$1 
./configure --prefix=$dir/install --host=$ARCH
if [ $? -ne 0 ]
then
	echo "configure fault! exit"
	exit
fi
make clean
make 
if [ $? -ne 0 ]
then
	echo "make fault! exit"
	exit
fi

make install
