#-------------------------------------------------
#
# Project created by QtCreator 2017-02-08T16:27:50
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = testboost
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += E:\qtwork\boost_1_56\bin\include

SOURCES += main.cpp

HEADERS += \
    event_proc.h
