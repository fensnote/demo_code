#include <QCoreApplication>
#include <iostream>

using namespace std;

#include "event_proc.h"
#include "boost/bind.hpp"

typedef boost::function<void(string, const void *, int)>  MSG_FUNCTION3;

//测试用的处理函数
void TestCmd(string event,const void * pData, int iLen)
{
    char *pcData = (char *)pData;
    cout<<"TestCmd: event: "<<event<<endl;

    if (pcData)
        printf("dev: %d, cmd: %d,dataLen: %d\n", pcData[0], pcData[1],iLen);
    return;
}

//测试用的处理函数
void TestCmd1(string event,const void * pData, int iLen)
{
    char *pcData = (char *)pData;
    cout<<"TestCmd1: event: "<<event<<endl;

    if (pcData)
        printf("dev: %d, cmd: %d,dataLen: %d\n", pcData[0], pcData[1],iLen);
    return;
}

//测试用类
class testFuncObject
{
public:
    void TestFunc(string event,const void * pData, int iLen)
    {
        cout<<"testFuncObject::TestFunc: event: "<<event<<endl;

        char *pcData = (char *)pData;
        if (pcData)
            printf("testFuncObject::TestFunc, dev: %d, cmd: %d,dataLen: %d\n", pcData[0], pcData[1],iLen);
    }
};


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    TSEventProc <string, MSG_FUNCTION3,const void*, int> eventMgr;
    char Data[32] = {0};

    eventMgr.RegEvent("event1",TestCmd);
    eventMgr.RegEvent("event1",TestCmd1);
    eventMgr.RegEvent("event2",TestCmd);

    //使用lambda表达式
    eventMgr.RegEvent("event2",[](string event,const void * pData, int iLen){
        cout<<"use labmda: event: "<<event<<endl;
    });

    //测试使用类成员函数
    testFuncObject testobj;
    eventMgr.RegEvent("event2",boost::bind(&testFuncObject::TestFunc, &testobj, _1,_2,_3));


    Data[0] = 31;
    Data[1] = 2;
    eventMgr.ProcEvent("event1", Data, (int)sizeof(Data));
    Data[1] = 3;
    eventMgr.ProcEvent("event2", Data, (int)sizeof(Data));

    return a.exec();
}
