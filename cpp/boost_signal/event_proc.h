/******************************************************************************

                  版权所有 (C), 2016-2026

 ******************************************************************************
  文 件 名   : event_proc.h
  版 本 号   : 初稿
  作    者   : wuquanwei
  生成日期   : 2017年1月7日
  最近修改   :
  功能描述   : 订阅发布实现模板
  函数列表   :
  修改历史   :
  1.日    期   : 2017年1月7日
    作    者   : wuquanwei
    修改内容   : 创建文件

******************************************************************************/
#ifndef __EVENT_PROC_H__
#define __EVENT_PROC_H__
#include <iostream>
#include <map>
#include <string>
#include <boost/bind.hpp>
#include "boost/signals2.hpp"

using namespace std;


#ifndef RTN_FAIL
#define RTN_FAIL -1
#endif

#ifndef RTN_SUCCESS
#define RTN_SUCCESS 0
#endif



/*----------------------------------类定义-----------------------------------*/
template <typename TypeEvent, typename TypeFunc ,typename TypeFnData, typename TypeFnDataLen>
class TSEventProc
{
public:
    TSEventProc() {}
    ~TSEventProc() {}
    int RegEvent(TypeEvent tpEvent, TypeFunc func);   // 注册处理函数
    int ProcEvent(TypeEvent tpEvent, TypeFnData tpData, TypeFnDataLen tpDataLen);  // 处理指令数据

    typedef boost::signals2::signal<void(TypeEvent,TypeFnData, TypeFnDataLen)> EventSignal;


protected:
    map< TypeEvent, EventSignal*> m_mpEventProcMap;   // 命令字与处理函数映射表
};


/*----------------------------------类实现-----------------------------------*/
//事件注册
template <typename TypeEvent,typename TypeFunc, typename TypeFnData, typename TypeFnDataLen>
int TSEventProc< TypeEvent, TypeFunc,TypeFnData, TypeFnDataLen>::RegEvent(TypeEvent tpEvent, TypeFunc func)
{
    typename map< TypeEvent, EventSignal* >::iterator iter = m_mpEventProcMap.find(tpEvent);
    if( iter != m_mpEventProcMap.end() )
    {
        iter->second->connect(func);
    }
    else
    {
        m_mpEventProcMap[tpEvent] = new EventSignal;
        m_mpEventProcMap[tpEvent]->connect(func);
    }

    return RTN_SUCCESS;
}

//事件处理
template <typename TypeEvent, typename TypeFunc,typename TypeFnData, typename TypeFnDataLen>
int TSEventProc<TypeEvent, TypeFunc,TypeFnData, TypeFnDataLen>::ProcEvent(TypeEvent tpEvent, TypeFnData tpData, TypeFnDataLen tpDataLen)
{
    EventSignal *pEventSignal = NULL;
    typename map< TypeEvent, EventSignal*>::iterator iter = m_mpEventProcMap.find(tpEvent);
    if(iter != m_mpEventProcMap.end())
    {
        pEventSignal = iter->second;
        (*pEventSignal)(tpEvent,tpData,tpDataLen);
    }
    else
    {
        cout<<"in ProcEvent, Can't find cmd ["<<tpEvent<<"] process function."<<endl;
        return RTN_FAIL;
    }

    return RTN_SUCCESS;
}


#endif /* __EVENT_PROC_H__ */

