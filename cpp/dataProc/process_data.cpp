#include "public.h"
#include "process_data.h"
#include <stdio.h>


/*****************************************************************************
 Prototype    : CDataProcess.CDataProcess
 Description  : 构造函数
 Input        : None
 Output       : None
 Return Value :

  History        :
  1.Date         : 2016/1/29
    Author       : fens
    Modification : Created function

*****************************************************************************/
CDataProcess::CDataProcess()
{

}
/*****************************************************************************
 Prototype    : CDataProcess.~CDataProcess
 Description  : 析构函数
 Input        : None
 Output       : None
 Return Value :

  History        :
  1.Date         : 2016/1/29
    Author       : fens
    Modification : Created function

*****************************************************************************/
CDataProcess::~CDataProcess()
{

}
/*****************************************************************************
 Prototype    : CDataProcess.registProcHandle
 Description  : 注册命令字处理函数对象
 Input        : string cmd
                MsgProcFunc handle
 Output       : None
 Return Value : int
 Calls        :
 Called By    :

  History        :
  1.Date         : 2016/1/29
    Author       : fens
    Modification : Created function

*****************************************************************************/
int CDataProcess::registProcHandle(string cmd,MsgProcFunc handle)
{
    map<string,MsgProcFunc>::iterator iter = devCmdActionMap.find(cmd);
    if ( iter ==  devCmdActionMap.end() )
    {
        devCmdActionMap[cmd] = handle;
        return 0;
    }
    else
    {
        printf("%s in %s, cmd: %s, alreay in the map\n",PRO_NAME, _FUN_, cmd.c_str());
    }

    return 0;
}

/*****************************************************************************
 Prototype    : CDataProcess.processDataFunction
 Description  : 数据处理
 Input        : string cmd
                const void *pData
                int iDataLen
 Output       : None
 Return Value : int

  History        :
  1.Date         : 2016/12/28
    Author       : fens
    Modification : Created function

*****************************************************************************/
int CDataProcess::processDataFunction(string cmd, const void *pData, int iDataLen)
{

    map<string,MsgProcFunc>::iterator iter = devCmdActionMap.find(cmd);
    if ( iter !=  devCmdActionMap.end() )
    {
        return iter->second(cmd,pData, iDataLen);
    }
    else
    {
        printf("%s in %s, not find cmd: %s func\n",PRO_NAME, _FUN_, cmd.c_str());
    }

    return -1;
}

/*****************************************************************************
 Prototype    : CDataProcess.getInstance
 Description  : 获取一个实例
 Input        : None
 Output       : None
 Return Value : CDataProcess

  History        :
  1.Date         : 2016/1/29
    Author       : fens
    Modification : Created function

*****************************************************************************/
CDataProcess *CDataProcess::getInstance()
{
	if (0 == m_instance)
	{
		m_instance = new CDataProcess();
	}
	return m_instance;
}

CDataProcess * CDataProcess::m_instance = 0;




