/******************************************************************************

  Copyright (C), 2001-2011, DCN Co., Ltd. wuquan-1230@163.com

 ******************************************************************************
  File Name     : tm_ui_process_data.h
  Version       : Initial Draft
  Author        : fens
  Created       : 2015/8/28
  Last Modified :
  Description   : tm_ui_process_data.cpp header file
  Function List :
  History       :
  1.Date        : 2015/8/28
    Author      : fens
    Modification: Created file

******************************************************************************/
#ifndef __TM_UI_PROCESS_DATA_H__
#define __TM_UI_PROCESS_DATA_H__

#include <iostream>
#include <map>
#include "public.h"
#include <boost/bind.hpp>
#include <boost/function.hpp>

using namespace std;

class CDataProcess;

//typedef int (CDataProcess::*DataMsgAction)(void *, int);

//boost bind def
typedef boost::function<int(string,const void *, int)>  MsgProcFunc;


class CDataProcess
{
private:

    map<string,MsgProcFunc > devCmdActionMap;

protected:
    CDataProcess();
    ~CDataProcess();
	static CDataProcess *m_instance;


public:
    static CDataProcess *getInstance();

    int registProcHandle(string cmd, MsgProcFunc handle);

    int processDataFunction(string cmd, const void *pData, int iDataLen);

};



#endif /* __TM_UI_PROCESS_DATA_H__ */
