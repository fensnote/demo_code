#include <iostream>
#include <stdio.h>
#include "process_data.h"

using namespace std;




int printHelpInfo(string cmd, const void  *pData, int dataLen)
{
	cout <<"-----------------------------------"<<endl;
	cout <<"help info:"<<endl;
	cout <<"help: print this info."<<endl;
	cout <<"ver : print this demo ver."<<endl;
	cout <<"test: process test class func cmd."<<endl;
	cout <<"q   : exit."<<endl;
	cout <<"-----------------------------------"<<endl;

    return 0;
}

int printVer(string cmd, const void  *pData, int dataLen)
{
	cout <<"in printVer, ver 1.0.0"<<endl;
    return 0;
}

class testDataProc
{
public:
	int TestDataProcFunc(string cmd, const void  *pData, int dataLen)
	{
		cout << "in testDataProc::TestDataProcFunc, process cmd "<<cmd<<endl;
		return 0;
	}
};

int main(int argc, char **argv)
{
	//��̬����ע��
	CDataProcess::getInstance()->registProcHandle("help", printHelpInfo);
	CDataProcess::getInstance()->registProcHandle("ver", printVer);

	//�������Ա����ע��
	testDataProc testProcObj;
	CDataProcess::getInstance()->registProcHandle("test",boost::bind(&testDataProc::TestDataProcFunc, testProcObj, _1, _2, _3));


	char cInPutBuf[256] = {0};
	while ( true )
	{
		printf("inPut cmd>: ");
        bzero(cInPutBuf, sizeof(cInPutBuf));
        cin.getline(cInPutBuf,sizeof(cInPutBuf));
		if (cInPutBuf[0] == 'q')
		{
			cout <<"Bye!"<<endl;
			break;
		}
		CDataProcess::getInstance()->processDataFunction(cInPutBuf, NULL, 0);
	}

	return 0;
}
