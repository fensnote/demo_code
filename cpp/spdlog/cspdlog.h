﻿#ifndef _CSPDLOG_H_
#define _CSPDLOG_H_


#include "spdlog/spdlog.h"
#include "spdlog/fmt/bin_to_hex.h"
#include <memory>
#include <string>

class CSpdlog
{
protected:
	
	CSpdlog();
	~CSpdlog();
	static CSpdlog *m_instance;
	
	
public:
	
	static CSpdlog *GetInstance();
	void Init(const std::string & name,const std::string &logPath, std::size_t max_size=1048576, std::size_t max_file = 2);
	void SetConsoleLogLevel(spdlog::level::level_enum log_level);
	void SetFileLogLevel(spdlog::level::level_enum log_level);

private:
	std::vector<spdlog::sink_ptr> m_sinks;
	std::shared_ptr<spdlog::logger> m_logger;
};










#endif

