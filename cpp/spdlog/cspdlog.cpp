﻿#include "cspdlog.h"

#include <cstdio>
#include <iostream>
#include "spdlog/sinks/stdout_color_sinks.h" // or "../stdout_sinks.h" if no color needed
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/rotating_file_sink.h"




CSpdlog::CSpdlog()
{
	
}

CSpdlog::~CSpdlog()
{
	
}

void CSpdlog::Init(const std::string & name, const std::string &log_path, std::size_t max_size, std::size_t max_file )
{
	try 
	{		
		auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
		console_sink->set_level(spdlog::level::debug);
		console_sink->set_pattern("%^[%Y-%m-%d %H:%M:%S:%e] [%n] [tid: %t] [%l] %v%$");

		std::string logFile = log_path + "/" + name + ".txt";
		
		//auto file_sink = std::make_shared<spdlog::sinks::basic_file_sink_mt>("logs/multisink.txt", false);
		auto file_sink = std::make_shared<spdlog::sinks::rotating_file_sink_mt>(logFile, max_size, max_file);
		file_sink->set_pattern("[%Y-%m-%d %H:%M:%S:%e] [%n] [tid: %t] [%l] %v");
		file_sink->set_level(spdlog::level::warn);
 		
		m_sinks.push_back(console_sink);
		m_sinks.push_back(file_sink);
		
		//spdlog::logger *logger = new spdlog::logger("multi_sink", {console_sink, file_sink});
		m_logger = std::make_shared<spdlog::logger>(name, begin( m_sinks ), end( m_sinks ));

		//spdlog::set_error_handler([](const std::string& msg){printf("*****Custom log error handler, %s*****%\n", msg.c_str());});
		
		//注册到spdlog里
		spdlog::register_logger(m_logger);
		//m_logger->info("log init done.");
		m_logger->flush_on(spdlog::level::level_enum::warn);	

	}
	catch (const spdlog::spdlog_ex &ex)
	{
		std::cout<<"Log initialization faild"<<ex.what()<<std::endl;
	}
}

void CSpdlog::SetConsoleLogLevel(spdlog::level::level_enum log_level)
{
	m_logger->set_level(log_level);
}

void CSpdlog::SetFileLogLevel(spdlog::level::level_enum log_level)
{
	m_sinks[1]->set_level(log_level);
}

CSpdlog* CSpdlog::m_instance = NULL;

CSpdlog* CSpdlog::GetInstance()
{
	if ( m_instance == NULL )
	{
		m_instance = new CSpdlog;
	}

	return m_instance;
}



