#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <cspdlog.h>

using namespace std;



int main()
{
	CSpdlog::GetInstance()->Init("test","./log"); //初始化日志
	CSpdlog::GetInstance()->SetConsoleLogLevel(spdlog::level::debug); //设置终端界面输出级别
	CSpdlog::GetInstance()->SetFileLogLevel(spdlog::level::warn);     //设置log文件输出级别
	
	auto logger = spdlog::get("test");   //获取日志句柄
	
	logger->warn("test start.");

	int counter = 0;	
	while(1)
	{
		logger->debug("debug msg, counter: {}",counter);
		logger->info("info msg, counter: {}",counter);
		logger->warn("warn msg, counter: {}",counter);
		logger->error("error msg, counter: {}",counter);
		logger->critical("critical msg, counter: {}",counter);
		logger->trace("trace msg, counter: {}",counter);
		usleep(500000);
	}

	return 0;
}
