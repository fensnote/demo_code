#include <iostream>
#include "json/json.h"
#include <string>


using namespace std;

int main()
{
    //1.创建json字符串
    cout << "1.creat json string----------------------------------------"<<endl;
    Json::Value jsonObj;

    jsonObj["id"]   = 1;     //int
    jsonObj["name"] = "Fens";//string
    jsonObj["age"]  = 18;    //int

    Json::Value jsonArray(Json::arrayValue); //creat an array
    jsonArray[0] = "13633838481"; //给数组添加数据
    jsonArray[1] = "15617051150";
    jsonObj["phone"]=jsonArray;     //将数组添加到对象中

    Json::StyledWriter styleWriter; //有个格式
    Json::FastWriter fastWriter;    //压缩，无格式
    cout << "styleWriter json string: "<<styleWriter.write(jsonObj)<<endl;
    cout << "-----------------------------------------------------------"<<endl;
    cout << "fastWriter json string: "<<fastWriter.write(jsonObj)<<endl;

    //2.解析json字符串
    cout << "2.decode json string----------------------------------------"<<endl;
    const string jsonString = "{\"id\":1,\"name\":\"fens\",\"phone\":[\"15617051150\",\"13833838481\"],\"school\":[{\"type\":\"primary\",\"name\":\"夏邑县第一实验小学\",\"address\":\"河南省夏邑县文化路\"},{\"type\":\"middle\",\"name\":\"夏邑县第三高级中学\",\"address\":\"河南省夏邑县建设路孔祖大道\"},{\"type\":\"college\",\"name\":\"郑州大学\",\"address\":\"河南省郑州市高新技术开发区科学大道\"}]}";

    Json::Reader reader;
    Json::Value  rootObj;
    reader.parse(jsonString, rootObj);

    cout <<"read json string: "<<rootObj.toStyledString()<<endl;



    return 0;
}
