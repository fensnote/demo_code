TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp


INCLUDEPATH += ./jsoncpp

DISTFILES += \
    ./jsoncpp/src/sconscript

SOURCES+=    ./jsoncpp/src/json_internalarray.inl \
    ./jsoncpp/src/json_internalmap.inl \
    ./jsoncpp/src/json_reader.cpp \
    ./jsoncpp/src/json_value.cpp \
    ./jsoncpp/src/json_valueiterator.inl \
    ./jsoncpp/src/json_writer.cpp
HEADERS+=    ./jsoncpp/src/json_batchallocator.h \
    ./jsoncpp/src/json_tool.h
