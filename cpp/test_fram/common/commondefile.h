﻿#ifndef COMMONDEFILE_H
#define COMMONDEFILE_H

//公共头文件，用于包含公共头文件、定义公共的宏定义、枚举、结构体等

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <map>
#include <thread>
#include <string>
#include <vector>



using namespace std;



//串口参数结构体，示例，根据实际参数修改
typedef struct UartInfo
{
    int portNo; //所属串口端口号
    int Baudrate; //波特率
    int startBit; //起始位
    int stopBit;  //结束位
    int parity;   //校验位
    int dataBit;  //数据位
}UartInfoSt;



//网络任务线程参数结构体，示例，根据实际参数修改
typedef struct NetInfo
{
    char ip[16]; //ip地址
    unsigned short port; //端口
    unsigned short addr; //链路地址
}NetInfoSt;
















#endif // COMMONDEFILE_H
