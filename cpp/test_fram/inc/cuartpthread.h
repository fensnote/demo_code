﻿/***************************************************************
*
*             fensnote@163.com
*
****************************************************************
* @file name:       CUartPthread
* @摘要              串口线程类
* @author           fensnote
* @date             2021-03-31
**************************************************************/
#ifndef INCCUARTPTHREAD_H
#define INCCUARTPTHREAD_H
#include "ctaskbase.h"
#include "commondefile.h"


class CUartPthread:public CTaskBase
{
public:
    CUartPthread();
    virtual ~CUartPthread();

public:
    virtual void init(const void *pArgs);
    virtual void start();
    virtual void stop();
    virtual bool state();

private:
    void taskThread();

private:
    UartInfoSt m_stUartInfo;
    bool m_run; //运行标志

    std::thread m_taskThread;
};

#endif // INCCUARTPTHREAD_H
