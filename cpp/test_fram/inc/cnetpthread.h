﻿#ifndef INCCNETPTHREAD_H
#define INCCNETPTHREAD_H
#include "ctaskbase.h"
#include "commondefile.h"


class CNetPthread:public CTaskBase
{
public:
    CNetPthread();
    virtual ~CNetPthread();

public:
    virtual void init(const void *pArgs);
    virtual void start();
    virtual void stop();
    virtual bool state();

private:
    void taskThread();

private:
    NetInfoSt m_stNetInfo;
    bool m_run; //运行标志

    std::thread m_taskThread;
};

#endif // INCCNETPTHREAD_H
