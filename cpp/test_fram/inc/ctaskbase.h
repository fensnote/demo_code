﻿/***************************************************************
*
*             fensnote@163.com
*
****************************************************************
* @file name:       CTaskBase
* @摘要              子任务线程基类（接口类）
* @author           fensnote
* @date             2021-03-31
**************************************************************/
#ifndef CTASKBASE_H
#define CTASKBASE_H


class CTaskBase
{
public:
    CTaskBase(){}
    virtual ~CTaskBase(){}

public:
    virtual void init(const void *pArgs) = 0; //初始化任务参数
    virtual void start() = 0;  //启动任务
    virtual void stop() = 0;   //结束任务
    virtual bool state() = 0;  //获取任务状态

};




#endif // CTASKBASE_H
