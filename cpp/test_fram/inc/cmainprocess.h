﻿/***************************************************************
*
*             fensnote@163.com
*
****************************************************************
* @file name:       CMainProcess类源文件
* @摘要              主处理类，用于 初始化整个程序，并启动整个程序里的各个子任务线程
* @author           fensnote
* @date             2021-03-31
**************************************************************/


#ifndef INCCMAINPROCESS_H
#define INCCMAINPROCESS_H

#include <iostream>
#include "commondefile.h"
#include "ctaskbase.h"


class CMainProcess
{
public:
    CMainProcess();
    ~CMainProcess();

public: //public function
    void init();
    void startService();

private:  //private function
    void startTask();

private:
    map<int , CTaskBase*>m_taskMap; //任务表，<端口号， 任务对象>
};

#endif // INCCMAINPROCESS_H
