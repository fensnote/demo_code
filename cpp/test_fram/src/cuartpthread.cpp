﻿#include "cuartpthread.h"
#include "commondefile.h"
#include "commonapi.h"

/***************************************************************
* @function name:  CUartPthread::CUartPthread
* @摘要             构造函数
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           fensnote
* @date             2021-03-31
**************************************************************/
CUartPthread::CUartPthread()
{
    printf("in CUartPthread::CUartPthread.\n");
    m_run = false;
}

/***************************************************************
* @function name:  CUartPthread::~CUartPthread
* @摘要             析构函数
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           fensnote
* @date             2021-03-31
**************************************************************/
CUartPthread::~CUartPthread()
{
    printf("in CUartPthread::~CUartPthread.\n");
}

/***************************************************************
* @function name:  CUartPthread::init
* @摘要             参数初始化
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           wuquanwei
* @date             2021-03-31
**************************************************************/
void CUartPthread::init(const void *pArgs)
{
    UartInfoSt *pstInfo = (UartInfoSt*)pArgs;
    memcpy(&m_stUartInfo, pstInfo, sizeof(UartInfoSt));

    printf("in CUartPthread::init, init port[%d].\n", m_stUartInfo.portNo);
}

/***************************************************************
* @function name:  CUartPthread::start
* @摘要             启动任务线程
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           wuquanwei
* @date             2021-03-31
**************************************************************/
void CUartPthread::start()
{
    if (m_run) //检测是否已经启动
    {
        return ;
    }

    printf("in CUartPthread::init, start port[%d] task thread.\n", m_stUartInfo.portNo);

    m_run = true;
    //创建并启动线程
    m_taskThread = std::thread(&CUartPthread::taskThread, this);
    m_taskThread.detach();//设置线程为分离模式，即结束后资源自动回收

}

/***************************************************************
* @function name:  CUartPthread::stop
* @摘要             停止任务线程
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           wuquanwei
* @date             2021-03-31
**************************************************************/
void CUartPthread::stop()
{
    m_run = false;
}

/***************************************************************
* @function name:  CUartPthread::state
* @摘要             返回当前任务状态
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           wuquanwei
* @date             2021-03-31
**************************************************************/
bool CUartPthread::state()
{
    return m_run;
}

/***************************************************************
* @function name:  CUartPthread::taskThread
* @摘要             线程体
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           wuquanwei
* @date             2021-03-31
**************************************************************/
void CUartPthread::taskThread()
{
    int counter = m_stUartInfo.portNo*1000; //测试
    char data[16] = "test data";

    while (m_run)
    {
        printf("in CUartPthread::taskThread, port[%d] task running.\n", m_stUartInfo.portNo);
        sleep(5);
        sendDataToDb(counter++, data, sizeof(data));  //模拟上传数据
    }
}
