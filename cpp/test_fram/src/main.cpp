﻿#include <iostream>
#include <cmainprocess.h>

using namespace std;

int main(int argc, char **argv)
{
	cout <<"in main start."<<endl;
	
    CMainProcess MainProcess;

    MainProcess.init();
    MainProcess.startService();

	return 0;
}
