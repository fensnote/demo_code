﻿#include "cmainprocess.h"
#include "commondefile.h"
#include "cuartpthread.h"
#include "cnetpthread.h"

/***************************************************************
* @function name:  CMainProcess::CMainProcess
* @摘要             构造函数
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           fensnote
* @date             2021-03-31
**************************************************************/
CMainProcess::CMainProcess()
{
    printf("in CMainProcess::CMainProcess.\n");
}

/***************************************************************
* @function name:  CMainProcess::~CMainProcess
* @摘要             析构函数
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           fensnote
* @date             2021-03-30
**************************************************************/
CMainProcess::~CMainProcess()
{
    printf("in CMainProcess::~CMainProcess.\n");

    //释放任务资源
    for( auto &iter : m_taskMap )
        delete iter.second;

    m_taskMap.clear();
}

/***************************************************************
* @function name:  CMainProcess::init
* @摘要             初始化配置，实例化子任务对象
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           fensnote
* @date             2021-03-30
**************************************************************/
void CMainProcess::init()
{
    printf("in CMainProcess::init.\n");

    UartInfoSt uartPara;
    CTaskBase *pTask = NULL;

    //模拟启动3个串口线程
    for ( int i =0 ; i < 3; i++ )
    {
        uartPara.portNo = i;
        pTask = new CUartPthread;
        pTask->init(&uartPara);

        m_taskMap[i] = pTask;  //将新建的任务线程对象添加到任务表里
    }

    NetInfoSt stNetInfo;

    //模拟启动2个串口线程
    for ( int i =0 ; i < 2; i++ )
    {
        strcpy(stNetInfo.ip , "192.168.1.11"); //填充测试参数
        stNetInfo.port = 10086+i;

        pTask = new CNetPthread; //实例化网络任务线程
        pTask->init(&stNetInfo);

        m_taskMap[stNetInfo.port] = pTask;  //将新建的任务线程对象添加到任务表里
    }



}

/***************************************************************
* @function name:  CMainProcess::startTask
* @摘要             启动子任务线程
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           wuquanwei
* @date             2021-03-31
**************************************************************/
void CMainProcess::startTask()
{
    printf("in CMainProcess::startTask, start sub task thread.\n");

    //循环启动任务列表里的所有任务
    for( auto &iter : m_taskMap )
        iter.second->start();


}

/***************************************************************
* @function name:  CMainProcess::startService
* @摘要             主线启动程服务
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           wuquanwei
* @date             2021-03-31
**************************************************************/
void CMainProcess::startService()
{
    printf("in CMainProcess::startService.\n");

    startTask();

    //主循环
    while (true)
    {
        // 检测各个任务的状态
        for( auto &iter : m_taskMap )
        {
            printf("in main loop, task[%d] state: %d\n", iter.first, iter.second->state());
        }
        sleep(10);
    }
}
