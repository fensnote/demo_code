﻿#include "cnetpthread.h"
#include "commonapi.h"

/***************************************************************
* @function name:  CNetPthread::CNetPthread
* @摘要             构造函数
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           fensnote
* @date             2021-03-31
**************************************************************/
CNetPthread::CNetPthread()
{
    printf("in CNetPthread::CNetPthread.\n");
    m_run = false;
}

/***************************************************************
* @function name:  CNetPthread::~CNetPthread
* @摘要             析构函数
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           fensnote
* @date             2021-03-31
**************************************************************/
CNetPthread::~CNetPthread()
{
    printf("in CNetPthread::~CNetPthread.\n");
}

/***************************************************************
* @function name:  CNetPthread::init
* @摘要             参数初始化
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           wuquanwei
* @date             2021-03-31
**************************************************************/
void CNetPthread::init(const void *pArgs)
{
    NetInfoSt *pstInfo = (NetInfoSt*)pArgs;
    memcpy(&m_stNetInfo, pstInfo, sizeof(NetInfoSt));

    printf("in CNetPthread::init, init net task[%s:%d].\n", m_stNetInfo.ip,m_stNetInfo.port);
}

/***************************************************************
* @function name:  CNetPthread::start
* @摘要             启动任务线程
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           wuquanwei
* @date             2021-03-31
**************************************************************/
void CNetPthread::start()
{
    if (m_run) //检测是否已经启动
    {
        return ;
    }

    printf("in CNetPthread::taskThread, start net task[%s:%d] task running.\n", m_stNetInfo.ip, m_stNetInfo.port);

    m_run = true;
    //创建并启动线程
    m_taskThread = std::thread(&CNetPthread::taskThread, this);
    m_taskThread.detach();//设置线程为分离模式，即结束后资源自动回收

}

/***************************************************************
* @function name:  CNetPthread::stop
* @摘要             停止任务线程
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           wuquanwei
* @date             2021-03-31
**************************************************************/
void CNetPthread::stop()
{
    m_run = false;
}

/***************************************************************
* @function name:  CNetPthread::state
* @摘要             返回当前任务状态
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           wuquanwei
* @date             2021-03-31
**************************************************************/
bool CNetPthread::state()
{
    return m_run;
}

/***************************************************************
* @function name:  CNetPthread::taskThread
* @摘要             线程体
* @输入参数          无
* @输出参数          无
* @返回值            void
* @author           wuquanwei
* @date             2021-03-31
**************************************************************/
void CNetPthread::taskThread()
{
    int counter = m_stNetInfo.port;
    char data[16] = "test data";
    while (m_run)
    {
        printf("in CNetPthread::taskThread, net task[%s:%d] task running.\n", m_stNetInfo.ip, m_stNetInfo.port);
        sleep(5);
        sendDataToDb(counter++, data, sizeof(data));//模拟上传数据
    }
}
