﻿/******************************************************************************

  Copyright (C), 2001-2011, DCN Co., Ltd.

 ******************************************************************************
  File Name     : commomParaSet.h
  Version       : Initial Draft
  Author        : fensnote
  Created       : 2010/1/10
  Last Modified :
  Description   : commomParaSet.cpp header file
  Function List :
  History       :
  1.Date        : 2010/1/10
    Author      : fensnote
    Modification: Created file

******************************************************************************/

#ifndef __COMMOMPARASET_H__
#define __COMMOMPARASET_H__

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//#include <linux/tcp.h>
#include <netinet/tcp.h>

#include <sys/un.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

#define UNIX_SOCKET_PATH  "/tmp/unix_sock"

#define SOCKET_SEND_RECV_TIMEOUT_SEC     10
#define SOCKET_SEND_RECV_TIMEOUT_USEC    0

typedef struct sockaddr    TSockAddr;
typedef struct sockaddr_un TSockAddrUn;
typedef struct sockaddr_in TSockAddrIn;
typedef struct linger      TSoLinger;




//connect返回值
#define RET_CONNECT_NOT_SET_ADDR    0
#define RET_CONNECT_FAULT          -1

int creat_pthreat(pthread_t* tidp, void*(*start_rtn)(void*),void * arg);

int setSocketAttr(int iFd);
int setObjectFdNoblock(int iFd);
int readable_timeo(int fd, int sec);



#endif /* __COMMOMPARASET_H__ */
