//file name: msg_test.c

#include "msg_test.h"

//创建消息队列
int Creat_msg(char *path, int id)
{
	key_t key;
	int msg_id;
	puts(path);
	if ((key = ftok(path, id)) == -1)
	{
		perror("ftok");
		exit(-1);
	}
	else 
	{
		printf("key = %d\n", key);
	}
	
	if ((msg_id = msgget(key, 0666 | IPC_CREAT)) == -1)
	{
		perror("msgget");
		exit(-1);
	}
	else 
	{
		printf("msg_id = %d\n", msg_id);
	}	
	return msg_id;
}
struct msg 
{
	long type;
	char buf[256+1];
};
int 
main(int argc, char **argv)
{
	int msgid = 0;
	int ret = 0;
	struct msg_struct msg_buf;
	struct msg buf1;
	bzero(&msg_buf, sizeof(msg_buf));
	
	if(argc != 3)
	{
		printf("Usag: ./%s path id\n", argv[0]);
		return 0;
	}
	
	printf("path : %s, id = %x\n", argv[1], atoi(argv[2]));
	msgid = Creat_msg(argv[1], atoi(argv[2]));
	printf("msgid = %d\n", msgid);
	
	buf1.type = 0x32;
#if 1
	while(1)
	{
		ret = msgsnd(msgid, (void *)&buf1, 256, 0); //IPC_NOWAIT
		//perror("msgrecv");
		printf("ret = %d, errno = %d\n", ret, errno);
		if(ret  < 0 )
		{
			printf("get ENOMSG\n");
			return 0;
		}
		usleep(100);
	}
#endif	
	if(msgctl(msgid, IPC_RMID, NULL)< 0)
	{
		perror("");
	}
	else 
	printf("rm OK!");
	return 0;
}

