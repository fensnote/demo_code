//file name: fifo_test.c
#include <sys/prctl.h>
#include "fifo_test.h"


int 
main(int argc, char **argv)
{
	int ret = 0;
	char buf[4] = {'\0'};
	int pipe_fd[2]; //0---read 1--write
	pid_t pid;
	
	if(pipe(pipe_fd)<0)
	{
		printf("pipe create error/n");
		return -1;
	}
	if((pid=fork())==0)
	{	
		close(pipe_fd[0]);
		prctl(PR_SET_NAME, "child");
		while(1)
		{	
			strncpy(buf, "hi!", sizeof("hi!"));
			ret=write(pipe_fd[1],buf,sizeof(buf));
			sleep(3);//
		}
	}

	close(pipe_fd[1]);
	while(1)
	{
		ret=read(pipe_fd[0],buf,sizeof(buf));
		puts(buf);
	}	

	return 0;
}

