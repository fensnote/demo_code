TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += E:\share_VM\hi3536c\xiangao\curl\curl\include
INCLUDEPATH += E:\share_VM\hi3536c\xiangao\curl\curl\include\curl
INCLUDEPATH += ../
INCLUDEPATH += E:\qtwork\boost_1_56\bin\include


SOURCES += ../main.cpp \
    ../chttp.cpp \
    ../cmd5.cpp

HEADERS += \
    ../chttp.h \
    ../cmd5.h \
    ../../app/inc/common_define.h

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../curl/win32/ -lcurl
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../curl/win32/ -lcurld


win32: LIBS += -L$$PWD/../curl/win32/ -lcurl


DEPENDPATH += $$PWD/../curl/win32
