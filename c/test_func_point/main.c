#include <stdio.h>
#include "test_msg_proc.h"

//函数指针
typedef void (*pfunc)(int , const void *, int );

//测试消息结构体
typedef struct testData
{
	int cmd;
	int len; //有效数据的长度
	char data[1024];
}testDataSt;

//消息处理节点信息结构体
typedef struct MsgProcNodeInfo
{
    int cmd;		  //消息名字
	char name[64];    //用于存储消息名字，方便调试
    pfunc func;   //处理回调函数
}MsgProcNodeInfoSt;

//消息处理注册表，注册命令字与其对应的处理函数
static  MsgProcNodeInfoSt msgMap[]=
{
	{0, "regCmd",   proc_msg_reg},
	{1, "loginCmd", proc_msg_login},
	{2, "testCmd",  proc_msg_test},
};

//处理函数
void proc_msg(int cmd, const void *pData, int len)
{
#if 1
	//处理
	int i = 0; 
	for ( i = 0; i < sizeof(msgMap)/sizeof(MsgProcNodeInfoSt); i++ )
	{
		if ( msgMap[i].cmd != cmd )
			continue;
				
		if ( msgMap[i].func == NULL )
			continue;
		
		msgMap[i].func(cmd, pData, len);
	}
	
	
#else 	
	//如果命令字正好与数组索引相同，也可以这样取巧,直接拿命令字作为索引用
	if ( cmd >= sizeof(msgMap)/sizeof(MsgProcNodeInfoSt) ) //越界判断
		return;
		
	msgMap[cmd].func(cmd, pData, len);
#endif	
}

int main(int argc, char **argv)
{
	proc_msg(0, "注册", sizeof("注册"));
	proc_msg(1, "登录", sizeof("登录"));
	proc_msg(2, "测试", sizeof("测试"));
	
	return 0;
}