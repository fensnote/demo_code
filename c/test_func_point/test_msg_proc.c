#include <stdio.h>
#include "test_msg_proc.h"


void proc_msg_reg(int cmd, const void *pData, int len)
{
	printf("in proc_msg_reg  , cmd: %d, data: %s, data len: %d\n", cmd, (const char *)pData, len);
	//处理数据
}

void proc_msg_login(int cmd, const void *pData, int len)
{
	printf("in proc_msg_login, cmd: %d, data: %s, data len: %d\n", cmd, (const char *)pData, len);
	//处理数据
}

void proc_msg_test(int cmd, const void *pData, int len)
{
	printf("in proc_msg_test , cmd: %d, data: %s, data len: %d\n", cmd, (const char *)pData, len);
	//处理数据
}
