#ifndef _UTF8_TO_UCS2_
#define _UTF8_TO_UCS2_

char* utf82unicode(const char* utf,size_t *unicode_number);

#endif