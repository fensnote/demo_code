#ifndef _SEND_MSH_H
#define _SEND_MSH_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>
//#include "safety_config.h"



#define GSM_7BIT		0
#define GSM_8BIT		4
#define GSM_UCS2		8

// 应答状态
#define GSM_WAIT		0		// 等待，不确定
#define GSM_OK			1		// OK
#define GSM_ERR			-1		// ERROR

// 短消息参数结构，编码/解码共用
// 其中，字符串以'\0'结尾
typedef struct {
	char SCA[16];			// 短消息服务中心号码(SMSC地址)
	char TPA[16];			// 目标号码或回复号码(TP-DA或TP-RA)
	char TP_PID;			// 用户信息协议标识(TP-PID)
	char TP_DCS;			// 用户信息编码方式(TP-DCS)
	char TP_SCTS[16];		// 服务时间戳字符串(TP_SCTS), 接收时用到
    char TP_UD[256];		// 原始用户信息(编码前或解码后的TP-UD)
	short index;			// 短消息序号，在读取时用到
} SM_PARAM;

int recv_msg(int fd);	//接收短信
unsigned short Unicode2GBcode(unsigned short iUnicode);
int strUnicode2GB(const unsigned char *strSourcer, char *strDest, int n);
int strGB2Unicode(const char *str, unsigned char *dst, int n);		//将汉字转换为Unicode编码
int String2Bytes(const char* pSrc, unsigned char* pDst, int nSrcLength); //将字符串转化为字节数据
int Bytes2String(const unsigned char* pSrc, char* pDst, int nSrcLength);
int convertNumbers(const char* pSrc, char* pDst, int nSrcLength);	//将手机号的奇偶位交换
int uconvertNumbers(const char* pSrc, char* pDst, int nSrcLength);	//与上一个函数功能相反
int Encode7bit(const char* pSrc, unsigned char* pDst, int nSrcLength);		//7bit编码
int Decode7bit(const unsigned char* pSrc, char* pDst, int nSrcLength);		//7bit解码
int Encode8bit(const char* pSrc, unsigned char* pDst, int nSrcLength);		//8bit编码
int Decode8bit(const unsigned char* pSrc, char* pDst, int nSrcLength);		//8bit解码
int EncodePdu(const SM_PARAM* pSrc, char* pDst);
int send_msg(int fd, char *msg, char *phone);		//发送短信



#endif