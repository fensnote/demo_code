#include "send_msg.h"
#include <stdio.h>
#include "uart.h"

int main(int argc, char **argv)
{
	if ( argc != 3 )
	{
		printf("Usage: %s <phone> <content>\n",argv[0]);
		return 0;
	}

	int fd = open_port(1);
	if( fd < 0)
	{
		printf("open port error!\n");
		return -1;
	}
	
	set_port( fd, 115200,8, 'N', 1);
	
	// char msg[512] = "一二三四五六七八九十一二三四五六七八九十一二三四五六七八九十一二三四五六七八九十一二三四五六七八";
	
	send_msg(fd, argv[2], argv[1]);
	
	return 0;
}



