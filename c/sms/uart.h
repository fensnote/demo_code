#ifndef _UART_H
#define _UART_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <termios.h>
#include <errno.h>

int open_port(int com_port);
int set_port(int fd, int baud_rate,int data_bits, char parity, int stop_bits);

#endif