######################################################################
# Automatically generated by qmake (3.0) ?? 7? 15 14:49:52 2021
######################################################################

TEMPLATE = app
TARGET = code
INCLUDEPATH += . inc
QT -= gui

# Input
HEADERS += inc/dom.h \
           inc/md5.h \
           inc/mecevp.h \
           inc/onvif.h \
           inc/smdevp.h \
           inc/soapH.h \
           inc/soapStub.h \
           inc/stdsoap2.h \
           inc/struct_timeval.h \
           inc/threads.h \
           inc/wsaapi.h \
           inc/wsdd.h \
           inc/wsseapi.h
SOURCES += inc/md5.c \
           src/dom.c \
           src/mecevp.c \
           src/smdevp.c \
           src/soapC.c \
           src/soapClient.c \
           src/stdsoap2.c \
           src/struct_timeval.c \
           src/threads.c \
           src/wsaapi.c \
           src/wsseapi.c \
    src/main.c
