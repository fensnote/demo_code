﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "soapH.h"
#include "stdsoap2.h"
#include "soapStub.h"
#include "wsseapi.h"

#include "wsdd.h" //命名空间

#define ONVIF_USER "admin"
#define ONVIF_PASSWORD "a1234567"
static struct soap* ONVIF_Initsoap(struct SOAP_ENV__Header *header, const char *was_To, const char *was_Action, int timeout)
{
    struct soap *soap = NULL;    // soap环境变量
    unsigned char macaddr[6];
    char _HwId[1024];
    unsigned int Flagrand;

    soap = soap_new();
    if(soap == NULL)
    {
        printf("[%d]soap = NULL\n", __LINE__);
        return NULL;
    }

    soap_set_namespaces(soap, namespaces);   // 设置soap的namespaces，即设置命名空间

    // 设置超时（超过指定时间没有数据就退出）
    if(timeout > 0)
    {
        soap->recv_timeout = timeout;
        soap->send_timeout = timeout;
        soap->connect_timeout = timeout;
    }
    else
    {
        //Maximum waittime : 20s
        soap->recv_timeout  = 20;
        soap->send_timeout  = 20;
        soap->connect_timeout = 20;
    }

    soap_default_SOAP_ENV__Header(soap, header);

    //Create SessionID randomly,生成uuid(windows下叫guid，linux下叫uuid)，格式为urn:uuid:8-4-4-4-12，由系统随机产生
    srand((int)time(0));
    Flagrand = rand()%9000 + 8888;
    macaddr[0] = 0x1;
    macaddr[1] = 0x2;
    macaddr[2] = 0x3;
    macaddr[3] = 0x4;
    macaddr[4] = 0x5;
    macaddr[5] = 0x6;
    sprintf(_HwId, "urn:uuid:%ud68a-1dd2-11b2-a105-%02X%02X%02X%02X%02X%02X", Flagrand, macaddr[0], macaddr[1], macaddr[2],macaddr[3],macaddr[4],macaddr[5]);
    header->wsa__MessageID = (char *)malloc(100);
    memset(header->wsa__MessageID, 0, 100);
    strncpy(header->wsa__MessageID, _HwId, strlen(_HwId));    //wsa__MessageID存放的是uuid

    if(was_Action != NULL)
    {
        header->wsa__Action = (char*)malloc(1024);
        memset(header->wsa__Action, '\0', 1024);
        strncpy(header->wsa__Action, was_Action, 1024); //
    }
    if(was_To != NULL)
    {
        header->wsa__To = (char *)malloc(1024);
        memset(header->wsa__To, '\0', 1024);
        strncpy(header->wsa__To, was_To, 1024);//"urn:schemas-xmlsoap-org:ws:2005:04:discovery";
    }
    soap->header = header;
    return soap;
}


//释放函数
void ONVIF_soap_delete(struct soap *soap)
{
    soap_destroy(soap);                      // remove deserialized class instances (C++ only)
    soap_end(soap);                          // Clean up deserialized data (except class instances) and temporary data
    soap_free(soap);                         // Reset and deallocate the context created with soap_new or soap_copy
}


//鉴权
static int ONVIF_SetAuthInfo(struct soap *soap, const char *username, const char *password)
{
    int result = 0;
    if((NULL != username) || (NULL != password)){
        soap_wsse_add_UsernameTokenDigest(soap, "user", username, password);
    }else{
        printf("un etAuth\n");
        result = -1;
    }

    return result;
}

int UserGetCapabilities(struct soap *soap	,
        struct _tds__GetCapabilities *capa_req,struct _tds__GetCapabilitiesResponse *capa_resp)
{
    char sercer_addr[] = "http://192.168.1.199/onvif/device_service"; //设备搜索得到的地址
    capa_req->Category = (enum tt__CapabilityCategory *)soap_malloc(soap, sizeof(int));
    capa_req->__sizeCategory = 1;
    *(capa_req->Category) = (enum tt__CapabilityCategory)(tt__CapabilityCategory__Media);

    capa_resp->Capabilities = (struct tt__Capabilities*)soap_malloc(soap,sizeof(struct tt__Capabilities)) ;

    int ret = soap_wsse_add_UsernameTokenDigest(soap,"user", ONVIF_USER, ONVIF_PASSWORD);
    printf("soap_wsse_add_UsernameTokenDigest ret: %d\n--------------------Now Gettting Capabilities NOW --------------------\n\n",ret);

    int result = soap_call___tds__GetCapabilities(soap, sercer_addr/*resp->wsdd__ProbeMatches->ProbeMatch->XAddrs*/, NULL, capa_req, capa_resp);

    if (soap->error)
    {
        int retval = soap->error;
        printf("[%s][%d]--->>> soap error: %d, %s, %s\n", __func__, __LINE__, retval, *soap_faultcode(soap), *soap_faultstring(soap));
        return -1;
    }
    else
    {
        printf(" \n--------------------GetCapabilities  OK! result=%d--------------\n \n",result);
        if(capa_resp->Capabilities==NULL)
        {
            printf(" GetCapabilities  failed!  result=%d \n",result);
            return -1;
        }
        else
        {
            printf(" Media->XAddr=%s \n", capa_resp->Capabilities->Media->XAddr);
            return 0;
        }
    }
}

int UserGetProfiles(struct soap *soap,struct _trt__GetProfiles *trt__GetProfiles,
        struct _trt__GetProfilesResponse *trt__GetProfilesResponse ,struct _tds__GetCapabilitiesResponse *capa_resp)
{
    int result=0 ;

    printf("\n-------------------Getting Onvif Devices Profiles--------------\n\n");
    soap_wsse_add_UsernameTokenDigest(soap,"user", ONVIF_USER, ONVIF_PASSWORD);
    result = soap_call___trt__GetProfiles(soap, capa_resp->Capabilities->Media->XAddr, NULL, trt__GetProfiles, trt__GetProfilesResponse);
    if (result==-1)
    //NOTE: it may be regular if result isn't SOAP_OK.Because some attributes aren't supported by server.
    //any question email leoluopy@gmail.com
    {
        printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));
        result = soap->error;
        return -1;
    }
    else{
        printf("\n-------------------Profiles Get OK--------------\n\n");
        if(trt__GetProfilesResponse->Profiles!=NULL)
        {
            if(trt__GetProfilesResponse->Profiles->Name!=NULL){
                printf("Profiles Name:%s  \n",trt__GetProfilesResponse->Profiles->Name);

            }
            if(trt__GetProfilesResponse->Profiles->token!=NULL){
                printf("Profiles Taken:%s\n",trt__GetProfilesResponse->Profiles->token);
            }
        }
        else{
            printf("Profiles Get inner Error\n");
            return -1;
        }
    }
    printf("Profiles Get Procedure over\n");

    return 0;
}

int UserGetUri(struct soap *soap,struct _trt__GetStreamUri *trt__GetStreamUri,struct _trt__GetStreamUriResponse *trt__GetStreamUriResponse,
         struct _trt__GetProfilesResponse *trt__GetProfilesResponse,struct _tds__GetCapabilitiesResponse *capa_resp)
{
    int result=0 ;
    trt__GetStreamUri->StreamSetup = (struct tt__StreamSetup*)soap_malloc(soap,sizeof(struct tt__StreamSetup));//初始化，分配空间
    trt__GetStreamUri->StreamSetup->Stream = tt__StreamType__RTP_Unicast;//stream type

    trt__GetStreamUri->StreamSetup->Transport = (struct tt__Transport *)soap_malloc(soap, sizeof(struct tt__Transport));//初始化，分配空间
    trt__GetStreamUri->StreamSetup->Transport->Protocol = tt__TransportProtocol__RTSP;
    trt__GetStreamUri->StreamSetup->Transport->Tunnel = 0;
    trt__GetStreamUri->StreamSetup->__size = 1;
    trt__GetStreamUri->StreamSetup->__any = NULL;
    trt__GetStreamUri->StreamSetup->__anyAttribute =NULL;


    trt__GetStreamUri->ProfileToken = trt__GetProfilesResponse->Profiles->token ;

    printf("\n\n---------------Getting Uri----------------\n\n");

    soap_wsse_add_UsernameTokenDigest(soap,"user", ONVIF_USER, ONVIF_PASSWORD);
    soap_call___trt__GetStreamUri(soap, capa_resp->Capabilities->Media->XAddr, NULL, trt__GetStreamUri, trt__GetStreamUriResponse);


    if (soap->error) {
    printf("soap error: %d, %s, %s\n", soap->error, *soap_faultcode(soap), *soap_faultstring(soap));
    result = soap->error;
    return -1;

    }
    else{
        printf("!!!!NOTE: RTSP Addr Get Done is :%s \n",trt__GetStreamUriResponse->MediaUri->Uri);
        return 0;
    }
}

int main(int argc,char *argv[])
{
    int ret = 0;
//    char sercer_addr[] = "http://192.168.1.200/onvif/device_service"; //设备搜索得到的地址



    struct SOAP_ENV__Header header;
    struct soap* soap = ONVIF_Initsoap(&header, NULL, NULL, 5);

    struct _tds__GetCapabilities *req;
    struct _tds__GetCapabilitiesResponse *Response;

    if(NULL == (req = (struct _tds__GetCapabilities *)calloc(1,sizeof(struct _tds__GetCapabilities))))
    {
        printf("calloc is error \n");
        ret = -1;
        return ret;
    }

    if(NULL == (Response = (struct _tds__GetCapabilitiesResponse *)calloc(1,sizeof(struct _tds__GetCapabilitiesResponse))))
    {
        printf("calloc is error \n");
        ret = -1;
        return ret;
    }

    ret = UserGetCapabilities(soap,req,Response);
    if ( ret != 0 )
    {
        printf("UserGetCapabilities  error \n");
        return ret;
    }

    struct _trt__GetProfiles trt__GetProfiles;
    struct _trt__GetProfilesResponse trt__GetProfilesResponse;

    ret = UserGetProfiles(soap,&trt__GetProfiles, &trt__GetProfilesResponse ,Response);
    if ( ret != 0 )
    {
        printf("UserGetProfiles  error \n");
        return ret;
    }

    struct _trt__GetStreamUri trt__GetStreamUri;
    struct _trt__GetStreamUriResponse trt__GetStreamUriResponse;

    ret = UserGetUri(soap,&trt__GetStreamUri,&trt__GetStreamUriResponse,&trt__GetProfilesResponse, Response);
    if ( ret != 0 )
    {
        printf("UserGetUri  error \n");
        return ret;
    }

    ONVIF_soap_delete(soap);
    return ret;
}
