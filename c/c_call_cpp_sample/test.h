#ifndef _TEST_H_
#define _TEST_H_

#ifdef __cplusplus
extern "C" {
#endif

void pushVal(int key, int val);
int getVal(int key );


#ifdef __cplusplus
}
#endif


#endif
