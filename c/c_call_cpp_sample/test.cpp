#include <map>
#include <iostream>
#include "test.h"

using namespace std;

static map<int, int> m_testMap;


void pushVal(int key, int val)
{
	m_testMap[key] = val;
}


int getVal(int key)
{
	map<int, int>::iterator iter = m_testMap.find(key);
	if (iter != m_testMap.end() )
	{
		return iter->second;
	}

	return  -1;
}
