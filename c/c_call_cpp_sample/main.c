#include <stdio.h>
#include "test.h"



int main()
{
	printf("test\n");
	for (int i = 0; i < 10; i++)
	{
		printf("push key: %d, val: %d\n", i, i*10);
		pushVal(i, i*10);
	}
	
	int val = 0;
	for (int i = 0; i < 10; i++)
	{
		val = getVal(i);
		printf("get key: %d, val: %d\n", i,val);
	}
	return 0;
}
